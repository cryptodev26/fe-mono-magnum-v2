const { dirScopes } = require('./scopes.config.js');

const setScopes = ['repo'];
const appScopes = dirScopes('./apps');
const packageScopes = dirScopes('./packages');

const scopes = setScopes.concat(appScopes, packageScopes);

module.exports = {
  extends: ['@commitlint/config-conventional', 'monorepo'],

  rules: {
    'scope-enum': [2, 'always', scopes],
    'type-enum': [
      2,
      'always',
      [
        'build',
        'chore',
        'config',
        'docs',
        'feat',
        'fix',
        'performance',
        'refactor',
        'revert',
        'style',
        'test',
      ],
    ],
  },
};
