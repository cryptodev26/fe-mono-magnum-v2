/** @type {import('@jest/types').Config.InitialOptions} */
const config = {
  clearMocks: false,
};

module.exports = config;
