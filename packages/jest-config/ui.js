/** @type {import('@jest/types').Config.InitialOptions} */
const config = {
  ...require('./common'),
  testEnvironment: 'jsdom',
  setupFilesAfterEnv: [
    '@testing-library/jest-dom/extend-expect',
    'jest-styled-components',
  ],
  moduleFileExtensions: ['js', 'jsx', 'json', 'ts', 'tsx'],
};

module.exports = config;
