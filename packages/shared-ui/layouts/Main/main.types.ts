import { ReactNode } from 'react';
import { NavProps } from '../../components';

export interface MainLayoutProps extends NavProps {
  children: ReactNode;
}
