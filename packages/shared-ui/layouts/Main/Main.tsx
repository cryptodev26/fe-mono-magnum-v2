import React from 'react';
import { Nav, Footer } from '../../components';
import { MainLayoutProps } from './main.types';

export const Main: React.FC<MainLayoutProps> = ({ children }) => {
  return (
    <>
      <Nav />
      {children}
      <Footer />
    </>
  );
};
