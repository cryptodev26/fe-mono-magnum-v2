// consider how many variants we'll want to make available

// general variants
export const fade = {
  hidden: {
    opacity: 0,
  },
  visible: {
    opacity: 1,
  },
};

export const parallex = {
  hidden: {
    ...fade.hidden,
    scale: 1.05,
    y: -3,
    x: 4,
  },
  visible: {
    ...fade.visible,
    scale: 1,
    y: 0,
    x: 0,
  },
};

export const slide = {
  hidden: {
    y: 1000,
    opacity: 0.5,
  },
  visible: {
    y: 0,
    opacity: 1,
  },
};

// component variants
export const modalSlide = {
  ...slide,
  visible: { ...slide.visible, y: '-50%' },
};

// transitions
export const defTransition = {
  duration: 0.25,
  delay: 0.15,
  ease: 'linear',
  delayChildren: 0.4,
  scatterChildren: 0.4,
};

export const easyTransition = {
  ...defTransition,
  duration: 0.4,
  ease: 'easeInOut',
};
