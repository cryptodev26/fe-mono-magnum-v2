import tw, { styled } from 'twin.macro';
import { motion } from 'framer-motion';

const styles = {
  active: tw`fixed w-full h-full top-0 left-0 bg-[rgba(0,0,0,.5)] transition-all duration-100 ease-linear z-10`,
};

const Dimmer = styled(motion.div)<{ $active: boolean }>(({ $active }) => [
  $active && styles.active,
]);

export default Dimmer;
