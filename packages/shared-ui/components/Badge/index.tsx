import tw, { styled, css, theme } from 'twin.macro';
import { classes } from '../../styles';

interface BadgeProps {
  variant?: 'primary' | 'secondary' | 'success' | 'danger' | 'neutral';
}

const styles = {
  base: tw`w-[fit-content] border bg-black text-white text-base px-1.5 sm:px-2 py-1 rounded-md transition-all duration-200 ease-in`,

  neutral: tw`border-0`,

  secondary: tw`border-neutral-700 text-neutral-700`,

  success: tw`border-green-500 text-green-500`,

  danger: tw`border-red-500 text-red-500`,

  primary: css`
    ${classes.gradientBorder}
    color: ${theme`colors.pink.500`};
  `,
};

const Badge = styled.div<BadgeProps>(({ variant = 'neutral' }) => [
  styles.base,
  variant === 'neutral' && styles.neutral,
  variant === 'primary' && styles.primary,
  variant === 'secondary' && styles.secondary,
  variant === 'success' && styles.success,
  variant === 'danger' && styles.danger,
]);

export type { BadgeProps };
export default Badge;
