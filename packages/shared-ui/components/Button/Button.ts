import tw, { styled, css } from 'twin.macro';
import { classes } from '../../styles';

interface ButtonProps {
  variant?: 'primary' | 'secondary' | 'text';
  social?: boolean;
}

const styles = {
  base: tw`text-white font-bold py-2.5 px-5 rounded-full transition-all duration-200 ease-in hover:-translate-y-0.5`,

  secondary: css`
    ${tw`border border-neutral-700 hover:border-neutral-600`}
    &:hover {
      ${classes.whiteGlow}
    }
  `,

  text: css`
    &:hover {
      ${classes.textGlow}
    }
  `,

  primary: css`
    ${classes.gradientBorder}
    &:hover {
      ${classes.gradientGlow}
    }
  `,
};

const Button = styled.button<ButtonProps>(({ variant = 'primary', social }) => [
  styles.base,
  variant === 'secondary' && styles.secondary,
  variant === 'text' && styles.text,
  !social && variant === 'primary' && styles.primary,
]);

export type { ButtonProps };
export default Button;
