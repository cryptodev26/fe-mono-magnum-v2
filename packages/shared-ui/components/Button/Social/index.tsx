import React, { ReactNode } from 'react';
import tw, { styled, css, theme } from 'twin.macro';
import Icon from '../../Icons';
import Button, { ButtonProps } from '../Button';

interface SocialButtonProps extends ButtonProps {
  icon:
    | 'twitter'
    | 'discord'
    | 'linkedin'
    | 'twitch'
    | 'instagram'
    | 'magicEden'
    | 'openSea'
    | 'youTube'
    | 'famousFox'
    | 'solScan';
  children: ReactNode;
  link?: string;
  onClick?: () => void;
}

const styles = {
  base: css`
    ${tw`w-full flex justify-center items-center hover:text-white`}
    border-radius: 50em;
    border: 1px solid transparent;
    &:hover {
      text-shadow: 0 0;
    }
  `,

  colorTheme: (colors: `#${string}`[]) => css`
    background: linear-gradient(
          ${theme`colors.neutral.900`},
          ${theme`colors.neutral.900`}
        )
        padding-box,
      linear-gradient(135deg, ${colors[0]}, ${colors[1]});
    &:hover {
      box-shadow: 0 0 5px #fff, -3px 0 10px ${colors[0]}, 3px 0 10px ${colors[1]},
        0 0 6px ${colors[1]}, -3px 0 9px ${colors[0]}, 3px 0 9px ${colors[0]};
    }
  `,
};

const StyledSocialButton = styled(Button)<SocialButtonProps>(({ icon }) => [
  styles.base,
  icon === 'twitter' && styles.colorTheme(['#1da1f2', '#0c81c9']),
  icon === 'discord' && styles.colorTheme(['#5865f2', '#3443ef']),
  icon === 'linkedin' && styles.colorTheme(['#0077b5', '#139cde']),
  icon === 'twitch' && styles.colorTheme(['#9146ff', '#6441a5']),
  icon === 'magicEden' && styles.colorTheme(['#900ce9', '#f94e9b']),
  icon === 'openSea' && styles.colorTheme(['#1868b7', '#15b2e5']),
  icon === 'youTube' && styles.colorTheme(['#ff0000', '#c50505']),
  icon === 'famousFox' && styles.colorTheme(['#ce6f3e', '#fb923c']),
  icon === 'solScan' && styles.colorTheme(['#c649e3', '#05e7b5']),
  icon === 'instagram' &&
    css`
      background: linear-gradient(
            ${theme`colors.neutral.900`},
            ${theme`colors.neutral.900`}
          )
          padding-box,
        linear-gradient(
          135deg,
          #285aeb 0%,
          #d6249f 5%,
          #fd5949 65%,
          #fdf497 95%,
          #fdf497 99%
        );
      &:hover {
        box-shadow: 0 0 5px #fff, -3px 0 10px #285aeb, 3px 0 10px #d6249f, 0 0 6px #fd5949,
          -3px 0 9px #fdf497, 3px 0 9px #fdf497;
      }
    `,
]);

const SocialButton: React.FC<SocialButtonProps> = ({
  icon,
  children,
  link = '#',
  ...props
}) => {
  return (
    <a
      href={link}
      target='_blank'
      rel='noopener noreferrer'
      tw='w-full'
    >
      <StyledSocialButton
        social
        icon={icon}
        {...props}
      >
        <div tw='flex justify-center items-center'>
          {children}
          <Icon
            variant={icon}
            tw='ml-2'
          />
        </div>
      </StyledSocialButton>
    </a>
  );
};

export type { SocialButtonProps };
export default SocialButton;
