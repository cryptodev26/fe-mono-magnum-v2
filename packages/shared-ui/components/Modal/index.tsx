import React, { ReactNode, useCallback, useEffect } from 'react';
import tw, { styled } from 'twin.macro';
import { motion } from 'framer-motion';

import Divider from '../Divider';
import Dimmer from '../Dimmer';
import Icon from '../Icons';
import { fade, modalSlide, defTransition } from '../../animations';

export interface ModalProps {
  heading?: string;
  open: boolean;
  handleClose: () => void;
  children: ReactNode;
}

const Container = styled(motion.div)`
  ${tw`fixed top-1/2 w-full h-full p-2.5 sm:(w-[600px] max-h-[95vh]) md:w-[760px] lg:w-[900px] xl:w-[1024px] shadow-xl z-50`}
`;

const Wrapper = styled.div`
  ${tw`p-2.5 border-0 rounded-md shadow-lg relative flex flex-col w-full h-full bg-neutral-900 outline-none focus:outline-none`}
`;

const Header = styled.div`
  ${tw`relative p-5 w-11/12 mx-auto rounded-t`}
`;

const Body = styled.div`
  ${tw`relative p-5 flex-auto w-11/12 mx-auto transition-all duration-300 ease-linear overflow-y-auto`}
`;

const Modal: React.FC<ModalProps> = ({ heading, open, handleClose, children }) => {
  const wrapperRef = React.useRef<HTMLDivElement>(null);

  // const handleBlur = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
  //   if (e.target === wrapperRef.current) handleClose();
  // };

  const handleBlur = useCallback(
    (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      if (e.target === wrapperRef.current) {
        // document.body.style.overflow = 'unset';
        handleClose();
        document.body.style.position = '';
        document.body.style.top = '';
      }
    },
    [handleClose]
  );

  useEffect(() => {
    if (open) {
      document.body.style.position = 'fixed';
      document.body.style.top = `-${window.scrollY}px`;
    }
  }, [open]);

  return (
    <>
      <Dimmer
        variants={fade}
        initial='hidden'
        animate='visible'
        exit='hidden'
        transition={defTransition}
        $active={open}
        onClick={handleBlur}
        ref={wrapperRef}
      />
      <Container
        variants={modalSlide}
        initial='hidden'
        animate='visible'
        exit='hidden'
        transition={defTransition}
      >
        <Wrapper>
          <Header>
            <h1>{heading}</h1>
            <Icon
              variant='close'
              tw='text-2xl cursor-pointer absolute top-3 -right-5'
              onClick={handleClose}
            />
            <Divider />
          </Header>
          <Body>{children}</Body>
        </Wrapper>
      </Container>
    </>
  );
};

export default Modal;
