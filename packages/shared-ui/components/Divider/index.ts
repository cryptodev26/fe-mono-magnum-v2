import tw, { styled } from 'twin.macro';

interface DividerProps {
  col?: boolean;
  row?: boolean;
}

const styles = {
  base: tw`transition-all duration-300 ease-linear`,

  row: tw`w-full h-[1px] my-5 bg-gradient-to-r from-purple-700  via-pink-500 to-neutral-900 `,

  col: tw`h-full w-[1px] mx-5 bg-gradient-to-b from-purple-700 via-pink-500 to-neutral-900`,
};

const Divider = styled.div<DividerProps>(({ col, row = true }) => [
  styles.base,
  row && styles.row,
  col && styles.col,
]);

export default Divider;
export type { DividerProps };
