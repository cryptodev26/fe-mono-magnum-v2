import tw, { styled, css, theme } from 'twin.macro';
import { motion } from 'framer-motion';

interface GridCellProps {
  col: number;
  sm?: number;
  md?: number;
  lg?: number;
  xl?: number;
}

const styles = {
  base: tw`grid grid-cols-12 gap-4`,

  cell: (col: number) =>
    css`
      grid-column: span ${col} / span ${col};
    `,
};

const Base = styled.div`
  ${styles.base}
`;

const Cell = styled(motion.div)<GridCellProps>(({ col, sm, md, lg, xl }) => [
  css`
    grid-column: span ${col} / span ${col};
  `,

  sm &&
    css`
      @media (min-width: ${theme`screens.sm`}) {
        ${styles.cell(sm)}
      }
    `,

  md &&
    css`
      @media (min-width: ${theme`screens.md`}) {
        ${styles.cell(md)}
      }
    `,

  lg &&
    css`
      @media (min-width: ${theme`screens.lg`}) {
        ${styles.cell(lg)}
      }
    `,

  xl &&
    css`
      @media (min-width: ${theme`screens.xl`}) {
        ${styles.cell(xl)}
      }
    `,
]);

const Grid = { Base, Cell };

export default Grid;
export type { GridCellProps };
