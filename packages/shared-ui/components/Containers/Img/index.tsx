import tw, { styled, css } from 'twin.macro';
import { classes } from '../../../styles';

interface ImgContainerProps {
  square?: boolean;
  circle?: boolean;
  featured?: boolean;
  glow?: boolean;
}

const styles = {
  base: tw`relative overflow-hidden w-full transition-all duration-300 ease-in rounded-md`,

  square: css`
    ${tw`before:block`}
    &:before {
      content: '';
      padding-top: 100%;
    }
    & img {
      ${tw`absolute top-1/2 left-1/2 -translate-y-1/2	-translate-x-1/2`}
    }
  `,

  circle: tw`rounded-full`,

  featured: css`
    ${classes.gradientBorder}
    &:hover {
      ${classes.gradientGlow}
    }
  `,
};

const Img = styled.div<ImgContainerProps>(({ square, circle, featured, glow }) => [
  styles.base,
  square && styles.square,
  circle && styles.circle,
  featured && styles.featured,
  glow && classes.gradientGlow,
]);

export default Img;
export type { ImgContainerProps };
