import Base from './Card.Base';
import Body from './Card.Body';
import Header from './Card.Header';
import Section from './Card.Section';

const Card = {
  Base,
  Body,
  Header,
  Section,
};

export default Card;
export type { CardProps, CardSectionProps } from './Card.types';
