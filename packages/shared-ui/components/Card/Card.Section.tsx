import tw, { styled } from 'twin.macro';
import { CardSectionProps } from './Card.types';

const styles = {
  base: tw`p-5 border border-neutral-700 rounded-md my-3 w-[fit-content] transition-all duration-300 ease-linear`,

  shaded: tw`bg-neutral-800 border-0`,
};

const Section = styled.section<CardSectionProps>(({ shaded }) => [
  styles.base,
  shaded && styles.shaded,
]);

export default Section;
