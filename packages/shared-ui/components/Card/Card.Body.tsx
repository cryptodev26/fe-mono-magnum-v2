import tw, { styled } from 'twin.macro';
import { CardProps } from './Card.types';

const styles = {
  base: tw`relative py-5 flex-auto w-11/12 mx-auto transition-all duration-300 ease-linear`,

  withImg: tw`w-auto px-0`,
};

const Body = styled.article<CardProps>(({ image }) => [
  styles.base,
  image && styles.withImg,
]);

export default Body;
