import { ReactNode } from 'react';

export interface CardProps {
  featured?: boolean;
  featuredInverse?: boolean;
  featuredSides?: boolean;
  image?: boolean;
  children?: ReactNode;
}

export interface CardSectionProps {
  shaded?: boolean;
}
