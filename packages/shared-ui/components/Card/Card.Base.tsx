import React from 'react';
import tw, { styled, css, theme } from 'twin.macro';
import { CardProps } from './Card.types';

const styles = {
  base: tw`w-full rounded-md p-2.5 bg-neutral-900 transition-all duration-300 ease-linear`,

  featured: (direction: 'top' | 'bottom') => css`
    background: linear-gradient(
      to ${direction},
      ${theme`colors.purple.700`} 0%,
      ${theme`colors.pink.500`} 33%,
      rgba(0, 0, 0, 0) 66%
    );
  `,

  sides: css`
    background: linear-gradient(
      rgba(0, 0, 0, 0) 0%,
      ${theme`colors.purple.700`} 33%,
      ${theme`colors.pink.500`} 50%,
      rgba(0, 0, 0, 0) 66%
    );
  `,

  inner: (featured?: boolean) => [
    tw`rounded-md bg-neutral-900 border border-neutral-700 py-5 md:p-2.5 transition-all duration-300 ease-linear`,
    featured && tw`border-0 shadow-2xl`,
  ],
};

const StyledBase = styled.div<CardProps>(
  ({ featured, featuredInverse, featuredSides }) => [
    styles.base,
    featured && styles.featured('bottom'),
    featuredInverse && styles.featured('top'),
    featuredSides && styles.sides,
  ]
);

const Inner = styled.div<CardProps>(({ featured }) => [
  styles.inner(),
  featured && styles.inner(featured),
]);

const Base: React.FC<CardProps> = ({
  featured,
  featuredInverse,
  featuredSides,
  children,
}) => (
  <StyledBase
    featured={featured}
    featuredInverse={featuredInverse}
    featuredSides={featuredSides}
  >
    <Inner featured={featured || featuredInverse || featuredSides}>{children}</Inner>
  </StyledBase>
);

export default Base;
