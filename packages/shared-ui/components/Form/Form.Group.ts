import tw, { styled } from 'twin.macro';

const Group = styled.div`
  ${tw`mx-3 my-5 flex flex-col`}
`;

export default Group;
