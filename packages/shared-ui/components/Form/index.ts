import CheckBox from './Form.CheckBox';
import FieldSet from './Form.Fieldset';
import Group from './Form.Group';
import Input from './Form.Input';
import Label from './Form.Label';
import Legend from './Form.Legend';
import Select from './Form.Select';
import Switch from './Form.Switch';
import TextArea from './Form.TextArea';

const Form = {
  CheckBox,
  FieldSet,
  Group,
  Input,
  Label,
  Legend,
  Select,
  Switch,
  TextArea,
};

export default Form;

export type { CheckBoxProps } from './Form.CheckBox';

export type { FormElementProps } from './Form.Input';
