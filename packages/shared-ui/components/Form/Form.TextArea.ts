import tw, { styled } from 'twin.macro';
import { classes } from '../../styles';

const TextArea = styled.textarea`
  ${tw`p-2.5 bg-neutral-800 rounded-md transition-[border] duration-300 delay-200 ease-linear outline-none`}
  &:focus-visible {
    ${tw`rounded-xl border border-transparent`}
    ${classes.gradientBorder}
      border-radius: 0.75rem;
    border: 1px solid transparent;
  }
`;

export default TextArea;
