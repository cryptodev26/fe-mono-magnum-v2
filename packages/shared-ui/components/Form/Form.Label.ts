import tw, { styled } from 'twin.macro';

const Label = styled.label`
  ${tw`mb-1.5`}
`;

export default Label;
