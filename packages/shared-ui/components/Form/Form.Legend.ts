import tw, { styled } from 'twin.macro';

const Legend = styled.legend`
  ${tw`px-2.5`}
`;

export default Legend;
