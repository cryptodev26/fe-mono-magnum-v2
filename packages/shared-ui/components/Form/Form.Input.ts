import { InputHTMLAttributes } from 'react';
import tw, { styled, css } from 'twin.macro';
import { classes } from '../../styles';

interface FormElementProps extends InputHTMLAttributes<HTMLInputElement> {
  invalid?: boolean;
}

const styles = {
  base: css`
    ${tw`p-2.5 bg-neutral-800 rounded-md transition-[border] duration-300 delay-200 ease-linear outline-none`}
    &:focus-visible {
      ${tw`rounded-xl border border-transparent`}
      ${classes.gradientBorder}
      border-radius: 0.75rem;
      border: 1px solid transparent;
    }
  `,

  invalid: tw`border border-red-500`,
};

const Input = styled.input<FormElementProps>(({ invalid }) => [
  styles.base,
  invalid && styles.invalid,
]);

export default Input;
export type { FormElementProps };
