/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { InputHTMLAttributes } from 'react';
import tw, { styled, css, theme } from 'twin.macro';
import Input from './Form.Input';

interface CheckBoxProps extends InputHTMLAttributes<HTMLInputElement> {
  lg?: boolean;
  active?: boolean;
}

const styles = {
  wrapper: tw`inline-flex items-center rounded-md overflow-hidden`,

  base: css`
    accent-color: ${theme`colors.purple.400`};
    ${tw`opacity-[.12] w-5 h-5 cursor-pointer bg-neutral-800 checked:opacity-100 border border-neutral-700 focus:ring-0 transition-colors duration-75 ease-linear`}
  `,

  lg: tw`w-7 h-7`,
};

const StyledCheckboxWrapper = styled.label`
  ${styles.wrapper}
`;

const StyledCheckBox = styled(Input)<CheckBoxProps>(({ lg }) => [
  styles.base,
  lg && styles.lg,
]);

const CheckBox = React.forwardRef<HTMLInputElement, CheckBoxProps>((props, checkRef) => {
  return (
    <div tw='block'>
      <StyledCheckboxWrapper>
        <StyledCheckBox
          type='checkbox'
          {...props}
          ref={checkRef}
        />
      </StyledCheckboxWrapper>
    </div>
  );
});

export default CheckBox;
export type { CheckBoxProps };
