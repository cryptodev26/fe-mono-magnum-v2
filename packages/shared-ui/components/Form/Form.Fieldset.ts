import tw, { styled } from 'twin.macro';

const FieldSet = styled.fieldset`
  ${tw`border border-neutral-700 rounded-md p-2.5 transition-[border] duration-300 delay-200 ease-linear`}
  & legend {
    ${tw`px-2.5`}
  }
`;

export default FieldSet;
