import tw, { styled, css } from 'twin.macro';
import { classes } from '../../styles';

const styles = {
  base: css`
    ${tw`p-2.5 bg-neutral-800 rounded-md transition-[border] duration-300 delay-200 ease-linear outline-none`}
    &:focus-visible {
      ${tw`rounded-xl border border-transparent outline-none`}
      ${classes.gradientBorder}
      border-radius: 0.75rem;
      border: 1px solid transparent;
    }
  `,
};

const Select = styled.select`
  ${styles.base}
`;

export default Select;
