import { theme } from 'twin.macro';

export const styles = {
  // Move long class sets out of jsx to keep it scannable
  border: () => `outline-width: 0;
  &:focus-visible {
    background: linear-gradient(
          ${theme`colors.neutral.900`},
          ${theme`colors.neutral.900`}
        )
        padding-box,
      linear-gradient(to left, ${theme`colors.purple.700`}, ${theme`colors.pink.500`})
        border-box;
    border-radius: 0.75rem;
    border: 1px solid transparent;
  }`,
};
