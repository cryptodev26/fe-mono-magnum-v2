import React from 'react';
import tw, { styled } from 'twin.macro';
import { classes } from '../../styles';

interface BulletProps {
  inProgress?: boolean;
  complete?: boolean;
}

const styles = {
  base: tw`flex justify-center items-center mx-5 w-4 h-4 rounded-full bg-gradient-to-b from-purple-500 to-pink-300 transition-all duration-300 ease-linear`,
  incomplete: tw`w-3 h-3 m-auto rounded-full bg-neutral-900`,
};

const StyledBullet = styled.div<BulletProps>(({ inProgress, complete }) => [
  styles.base,
  (inProgress || complete) && classes.gradientGlow,
]);

const Bullet = ({ complete, inProgress }: BulletProps) => (
  <StyledBullet
    complete={complete}
    inProgress={inProgress}
  >
    {!complete && <div css={styles.incomplete} />}
  </StyledBullet>
);

export type { BulletProps };
export default Bullet;
