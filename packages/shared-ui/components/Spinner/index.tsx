import React from 'react';
import 'twin.macro';
import Dimmer from '../Dimmer';

const Spinner = () => (
  <Dimmer
    $active
    tw='w-screen h-screen flex justify-center items-center'
  >
    <div tw='w-32 flex justify-center items-center'>
      <picture>
        <source
          src='https://magnum-images.s3.amazonaws.com/brand/spinner.gif'
          type='image/webp'
        />
        <img
          src='https://magnum-images.s3.amazonaws.com/brand/spinner.gif'
          alt='loading...'
        />
      </picture>
    </div>
  </Dimmer>
);

export default Spinner;
