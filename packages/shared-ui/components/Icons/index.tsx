import React, { SVGProps } from 'react';
import * as icons from './components';

type IconList =
  | 'add'
  | 'back'
  | 'close'
  | 'discord'
  | 'famousFox'
  | 'instagram'
  | 'linkedin'
  | 'magicEden'
  | 'next'
  | 'openSea'
  | 'settings'
  | 'solana'
  | 'solScan'
  | 'spinner'
  | 'subtract'
  | 'twitch'
  | 'twitter'
  | 'wallet'
  | 'youTube';

type Icons =
  | 'Add'
  | 'Back'
  | 'Close'
  | 'Discord'
  | 'FamousFox'
  | 'Instagram'
  | 'Linkedin'
  | 'MagicEden'
  | 'Next'
  | 'OpenSea'
  | 'Settings'
  | 'Solana'
  | 'SolScan'
  | 'Spinner'
  | 'Subtract'
  | 'Twitch'
  | 'Twitter'
  | 'Wallet'
  | 'YouTube';

interface IconProps extends SVGProps<any> {
  variant: IconList;
}

const Icon = ({ variant, ...props }: IconProps) => {
  if (!variant) return null;
  const capitalized: string = (variant as string)
    .split('')
    .map((letter: string, i: number) => (i === 0 ? letter.toUpperCase() : letter))
    .join('');

  return React.createElement(icons[capitalized as Icons], {
    ...props,
  });
};

export default Icon;
export type { IconList, Icons, IconProps };
