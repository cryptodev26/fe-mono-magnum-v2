import React from 'react';
import { JSX } from '@emotion/react/jsx-runtime';

const Solana = (props: JSX.IntrinsicAttributes) => (
  <svg
    id='a'
    xmlns='http://www.w3.org/2000/svg'
    xmlnsXlink='http://www.w3.org/1999/xlink'
    viewBox='0 0 30.62 24'
    width='1em'
    height='1em'
    {...props}
  >
    <defs>
      <linearGradient
        id='b'
        x1='27.79'
        y1='-292.88'
        x2='10.87'
        y2='-260.49'
        gradientTransform='translate(0 290)'
        gradientUnits='userSpaceOnUse'
      >
        <stop
          offset='0'
          stopColor='#00ffa3'
        />
        <stop
          offset='1'
          stopColor='#dc1fff'
        />
      </linearGradient>
      <linearGradient
        id='c'
        x1='20.39'
        y1='-296.74'
        x2='3.48'
        y2='-264.35'
        gradientTransform='translate(0 290)'
        gradientUnits='userSpaceOnUse'
      >
        <stop
          offset='0'
          stopColor='#00ffa3'
        />
        <stop
          offset='1'
          stopColor='#dc1fff'
        />
      </linearGradient>
      <linearGradient
        id='d'
        x1='24.07'
        y1='-294.83'
        x2='7.15'
        y2='-262.43'
        gradientTransform='translate(0 290)'
        gradientUnits='userSpaceOnUse'
      >
        <stop
          offset='0'
          stopColor='#00ffa3'
        />
        <stop
          offset='1'
          stopColor='#dc1fff'
        />
      </linearGradient>
    </defs>
    <path
      fill='url(#b)'
      d='M4.98,18.32c.18-.18,.44-.29,.71-.29H30.12c.45,0,.67,.54,.35,.85l-4.83,4.83c-.18,.18-.44,.29-.71,.29H.5c-.45,0-.67-.54-.35-.85l4.83-4.83Z'
    />
    <path
      fill='url(#c)'
      d='M4.98,.29c.19-.18,.45-.29,.71-.29H30.12c.45,0,.67,.54,.35,.85l-4.83,4.83c-.18,.18-.44,.29-.71,.29H.5c-.45,0-.67-.54-.35-.85L4.98,.29Z'
    />
    <path
      fill='url(#d)'
      d='M25.65,9.25c-.18-.18-.44-.29-.71-.29H.5c-.45,0-.67,.54-.35,.85l4.83,4.83c.18,.18,.44,.29,.71,.29H30.12c.45,0,.67-.54,.35-.85l-4.83-4.83Z'
    />
  </svg>

  // <svg
  //   xmlns='http://www.w3.org/2000/svg'
  //   viewBox='0 0 397.7 311.7'
  //   xmlSpace='preserve'
  //   width='1em'
  //   height='1em'
  //   {...props}
  // >
  //   <linearGradient
  //     id='a'
  //     gradientUnits='userSpaceOnUse'
  //     x1={360.879}
  //     y1={351.455}
  //     x2={141.213}
  //     y2={-69.294}
  //     gradientTransform='matrix(1 0 0 -1 0 314)'
  //   >
  //     <stop
  //       offset={0}
  //       style={{
  //         stopColor: '#00ffa3',
  //       }}
  //     />
  //     <stop
  //       offset={1}
  //       style={{
  //         stopColor: '#dc1fff',
  //       }}
  //     />
  //   </linearGradient>
  //   <path
  //     d='M64.6 237.9c2.4-2.4 5.7-3.8 9.2-3.8h317.4c5.8 0 8.7 7 4.6 11.1l-62.7 62.7c-2.4 2.4-5.7 3.8-9.2 3.8H6.5c-5.8 0-8.7-7-4.6-11.1l62.7-62.7z'
  //     style={{
  //       fill: 'url(#a)',
  //     }}
  //   />
  //   <linearGradient
  //     id='b'
  //     gradientUnits='userSpaceOnUse'
  //     x1={264.829}
  //     y1={401.601}
  //     x2={45.163}
  //     y2={-19.148}
  //     gradientTransform='matrix(1 0 0 -1 0 314)'
  //   >
  //     <stop
  //       offset={0}
  //       style={{
  //         stopColor: '#00ffa3',
  //       }}
  //     />
  //     <stop
  //       offset={1}
  //       style={{
  //         stopColor: '#dc1fff',
  //       }}
  //     />
  //   </linearGradient>
  //   <path
  //     d='M64.6 3.8C67.1 1.4 70.4 0 73.8 0h317.4c5.8 0 8.7 7 4.6 11.1l-62.7 62.7c-2.4 2.4-5.7 3.8-9.2 3.8H6.5c-5.8 0-8.7-7-4.6-11.1L64.6 3.8z'
  //     style={{
  //       fill: 'url(#b)',
  //     }}
  //   />
  //   <linearGradient
  //     id='c'
  //     gradientUnits='userSpaceOnUse'
  //     x1={312.548}
  //     y1={376.688}
  //     x2={92.882}
  //     y2={-44.061}
  //     gradientTransform='matrix(1 0 0 -1 0 314)'
  //   >
  //     <stop
  //       offset={0}
  //       style={{
  //         stopColor: '#00ffa3',
  //       }}
  //     />
  //     <stop
  //       offset={1}
  //       style={{
  //         stopColor: '#dc1fff',
  //       }}
  //     />
  //   </linearGradient>
  //   <path
  //     d='M333.1 120.1c-2.4-2.4-5.7-3.8-9.2-3.8H6.5c-5.8 0-8.7 7-4.6 11.1l62.7 62.7c2.4 2.4 5.7 3.8 9.2 3.8h317.4c5.8 0 8.7-7 4.6-11.1l-62.7-62.7z'
  //     style={{
  //       fill: 'url(#c)',
  //     }}
  //   />
  // </svg>
);

export default Solana;
