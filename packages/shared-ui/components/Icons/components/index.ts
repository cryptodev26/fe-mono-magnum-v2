export { default as Add } from './Icon.Add';

export { default as Back } from './Icon.Back';

export { default as Close } from './Icon.Close';

export { default as Discord } from './Icon.Discord';

export { default as FamousFox } from './Icon.FamousFox';

export { default as Instagram } from './Icon.Instagram';

export { default as Linkedin } from './Icon.Linkedin';

export { default as MagicEden } from './Icon.MagicEden';

export { default as Next } from './Icon.Next';

export { default as OpenSea } from './Icon.OpenSea';

export { default as Settings } from './Icon.Settings';

export { default as SolScan } from './Icon.SolScan';

export { default as Solana } from './Icon.Solana';

export { default as Spinner } from './Icon.Spinner';

export { default as Subtract } from './Icon.Subtract';

export { default as Twitch } from './Icon.Twitch';

export { default as Twitter } from './Icon.Twitter';

export { default as Wallet } from './Icon.Wallet';

export { default as YouTube } from './Icon.YouTube';
