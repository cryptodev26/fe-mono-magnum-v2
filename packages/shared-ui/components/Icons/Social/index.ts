export { default as SocialIconLink } from './Icon.Social.Link';

export { default as SocialIcon } from './Icon.Social';
export type { SocialData } from './Icon.Social';

export { socialList } from './Icon.Social.data';
