import tw, { styled } from 'twin.macro';

import Icon from '..';

interface SocialData {
  variant: 'discord' | 'twitter' | 'youTube' | 'magicEden' | 'openSea';
  to?: string;
}

const styles = {
  twitter: tw`hover:text-[#1da1f2] transition-all duration-200 ease-in mx-1.5`,

  discord: tw`hover:text-[#5865f2] transition-all duration-200 ease-in mx-1.5`,

  magicEden: tw`hover:text-[#f94e9b] transition-all duration-200 ease-in mx-1.5`,

  youTube: tw`hover:text-[#ff0000] transition-all duration-200 ease-in mx-1.5`,

  openSea: tw`hover:text-[#15b2e5] transition-all duration-200 ease-in mx-1.5`,
};

const SocialIcon = styled(Icon)<SocialData>(({ variant }) => [
  variant === 'twitter' && styles.twitter,
  variant === 'discord' && styles.discord,
  variant === 'magicEden' && styles.magicEden,
  variant === 'youTube' && styles.youTube,
  variant === 'openSea' && styles.openSea,
]);

export default SocialIcon;
export type { SocialData };
