import React from 'react';
import SocialIcon, { SocialData } from './Icon.Social';

const SocialIconLink = ({ to, variant }: SocialData) => {
  return (
    <a
      href={to}
      target='_blank'
      rel='noopener noreferrer'
    >
      <SocialIcon variant={variant} />
    </a>
  );
};

export default SocialIconLink;
