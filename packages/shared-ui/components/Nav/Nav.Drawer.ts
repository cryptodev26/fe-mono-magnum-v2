import tw, { styled } from 'twin.macro';

interface SideNavProps {
  active: boolean;
}

const styles = {
  base: tw`fixed bottom-0 right-[-100%] w-full md:w-1/2 lg:w-1/3 bg-neutral-900 border-l border-neutral-700 transition-all duration-300 ease-linear flex flex-col justify-between z-10`,

  active: tw`right-0`,
};

const Drawer = styled.div<SideNavProps>(({ active }) => [
  styles.base,
  active && styles.active,
]);

export default Drawer;
export type { SideNavProps };
