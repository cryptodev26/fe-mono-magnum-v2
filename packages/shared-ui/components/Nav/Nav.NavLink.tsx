import React, { ReactNode } from 'react';
import 'twin.macro';

interface NavLinkProps {
  to: string;
  children: ReactNode;
}

const NavLink: React.FC<NavLinkProps> = ({ to, children }) => {
  return (
    <a
      href={to}
      tw='w-full p-5'
    >
      <div tw='w-full'>{children}</div>
    </a>
  );
};

export default NavLink;
export type { NavLinkProps };
