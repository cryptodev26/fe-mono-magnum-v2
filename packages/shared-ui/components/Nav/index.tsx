import React, { FC, useState, useRef, useEffect } from 'react';
import 'twin.macro';

import Base from './Nav.Base';
import Logo from './Nav.Logo';
import Dimmer from '../Dimmer';
/* import Drawer from './Nav.Drawer';
import Hamburger from './Nav.Hamburger';
import { Button } from '../Button';
import { SocialIconLink, socialList } from '../Icons/Social'; */

interface NavProps {
  button?: JSX.Element;
  // children?: ReactNode;
}

const Nav: FC<NavProps> = ({ button /* children */ }) => {
  const navRef = useRef<HTMLElement | null>(null);
  // const sideRef = useRef() as React.MutableRefObject<HTMLDivElement>;
  const [activeBurger, setActiveBurger] = useState<boolean>(false);
  const [, /* height */ setHeight] = useState<number>(0);
  const [sticky, setSticky] = useState(false);

  useEffect(() => {
    const navHeightSetter = () => {
      if (navRef.current) {
        setHeight(navRef.current.offsetHeight);
      }
    };
    navHeightSetter();
  }, [navRef]);

  useEffect(() => {
    const scrollHandler = () => {
      if (window.scrollY > 20) setSticky(true);
      else setSticky(false);
    };
    window.addEventListener('scroll', scrollHandler);
    return () => window.removeEventListener('scroll', scrollHandler);
  });

  return (
    <>
      <Base
        ref={navRef}
        sticky={sticky}
      >
        <Logo />
        <div tw='flex items-center'>
          <div tw='block mr-5'>{button}</div>
          {/* <Button
            variant='secondary'
            tw='p-2 rounded-md ml-3 hover:translate-y-0'
            onClick={() => {
              setActiveBurger(!activeBurger);
              if (activeBurger) sideRef.current.focus();
            }}
          >
            <Hamburger active={activeBurger} />
          </Button> */}
        </div>
        {/* <Drawer
          ref={sideRef}
          active={activeBurger}
          style={{
            height: height !== 0 ? `calc(100vh - ${height}px)` : 'calc(100vh - 5rem)',
          }}
          onBlur={e =>
            !e.currentTarget.contains(e.relatedTarget) && setActiveBurger(false)
          }
        >
          <div>{children}</div>
          <div tw='md:ml-auto w-1/2 flex justify-around'>
            {socialList.map(social => (
              <SocialIconLink
                key={social.variant}
                to={social.to}
                variant={social.variant}
              />
            ))}
          </div>
        </Drawer> */}
      </Base>
      <Dimmer
        $active={activeBurger}
        onClick={() => setActiveBurger(false)}
      />
    </>
  );
};

export default Nav;
export type { NavProps };

export type { BaseNavProps } from './Nav.Base';
export type { SideNavProps } from './Nav.Drawer';
export type { NavLinkProps } from './Nav.NavLink';
