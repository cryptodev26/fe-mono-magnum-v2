import React from 'react';
import 'twin.macro';

const Logo = () => (
  <a
    href='/'
    onClick={() => window.scrollTo(0, 0)}
    tw='w-1/3 md:w-1/4 lg:w-1/5 xl:w-1/6'
  >
    <picture>
      <source
        src='https://magnum-images.s3.amazonaws.com/brand/brand.png'
        type='image/webp'
      />
      <img
        src='https://magnum-images.s3.amazonaws.com/brand/brand.png'
        alt='magnum'
      />
    </picture>
  </a>
);

export default Logo;
