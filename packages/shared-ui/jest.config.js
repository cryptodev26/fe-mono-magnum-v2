/** @type {import('@jest/types').Config.InitialOptions} */
const config = {
  ...require('jest-config/ui'),
  rootDir: '../../',
};

module.exports = config;
