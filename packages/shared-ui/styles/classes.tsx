import { css, theme } from 'twin.macro';

export const classes = {
  gradientBorder: css`
    background: linear-gradient(
          ${theme`colors.neutral.900`},
          ${theme`colors.neutral.900`}
        )
        padding-box,
      linear-gradient(to left, ${theme`colors.purple.700`}, ${theme`colors.pink.500`})
        border-box;
    border: 1px solid transparent;
  `,

  gradientGlow: css`
    box-shadow: 0 0 5px #fff, -3px 0 10px ${theme`colors.pink.500`},
      3px 0 10px ${theme`colors.purple.500`}, 0 0 6px ${theme`colors.purple.500`},
      -3px 0 9px ${theme`colors.pink.500`}, 3px 0 9px ${theme`colors.pink.600`};
  `,

  whiteGlow: css`
    box-shadow: 0 0 5px #fff, -3px 0 10px ${theme`colors.neutral.500`},
      3px 0 10px ${theme`colors.neutral.500`}, 0 0 6px ${theme`colors.neutral.500`},
      -3px 0 9px ${theme`colors.neutral.500`}, 3px 0 9px ${theme`colors.neutral.600`};
  `,

  textGlow: css`
    text-shadow: 0 0 1px ${theme`colors.neutral.50`}, 0 0 2px ${theme`colors.neutral.50`},
      0 0 1px ${theme`colors.neutral.50`}, 0 0 3px ${theme`colors.neutral.50`},
      0 0 1px ${theme`colors.neutral.200`}, 0 0 4px ${theme`colors.neutral.200`};
  `,
};
