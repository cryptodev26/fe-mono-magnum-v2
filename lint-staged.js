module.exports = {
  '*.{ts,tsx}': ['eslint'], // modify for max warnings when ready
  '*.{ts,tsx,json,css,js}': ['prettier --write'],
};
