const { readdirSync: readDirectory } = require('fs');

exports.dirScopes = path =>
  readDirectory(path, { withFileTypes: true })
    .filter(entry => entry.isDirectory())
    .map(dir => `${dir.name}[${path.slice(2) === 'apps' ? 'app' : 'pkg'}]`);
