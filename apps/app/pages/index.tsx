/* eslint-disable @typescript-eslint/ban-ts-comment */
import React from 'react';
import tw from 'twin.macro';
import { Bullet } from 'shared-ui';

const styles = {
  // Move long class sets out of jsx to keep it scannable
  container: ({ hasBackground }: { hasBackground: boolean }) => [
    tw`flex flex-col items-center justify-center h-screen`,
    hasBackground && tw`bg-black`,
  ],
};

const App = () => (
  // @ts-ignore TODO: resolve t2322 fir css and tw props
  <div css={styles.container({ hasBackground: true })}>
    {/* @ts-ignore TODO: resolve t2322 fir css and tw props */}
    <div tw='flex flex-col justify-center h-full gap-y-5'>
      {/* <Button variant="primary">Submit</Button>
      <Button variant="secondary">Cancel</Button>
      <Button isSmall>Close</Button> */}
      <Bullet complete />
    </div>
    {/* <Logo /> */}
  </div>
);

export default App;
