import { Transaction } from '@solana/web3.js';

import { TransactionInit, PublicKey } from 'types';

export const initTransaction: TransactionInit = async (program, wallet) => {
  const {
    provider: { connection },
  } = program;

  const transaction = new Transaction();
  const blockhash = await connection.getLatestBlockhash();

  transaction.recentBlockhash = blockhash.blockhash;
  transaction.feePayer = wallet.publicKey as PublicKey;

  return transaction;
};
