import { PublicKey } from '@solana/web3.js';

import legendaries from 'data/legendaries.json';
import { GetNFT } from 'types/types.nft';
import { getMetaDataByMint } from './getMetaData';

export const getNft: GetNFT = async (
  connection,
  mint,
  assTokenAccount,
  stakedAccount,
  stakedAccountPublicKey
) => {
  try {
    const { data, info, metadata } = await getMetaDataByMint(connection, mint);

    return {
      mint: new PublicKey(data?.mint as string) as PublicKey,
      owner: info?.owner as PublicKey,
      metadata: metadata.data,
      isLegendary: legendaries.includes(mint.toString()),
      stakedAccountPublicKey,
      assTokenAccount,
      stakedAccount,
    };
  } catch (err) {
    console.error(`error getting nft: `, err);
  }
};
