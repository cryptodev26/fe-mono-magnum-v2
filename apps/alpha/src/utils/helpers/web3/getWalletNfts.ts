import hashes from 'data/hashes.json';
import { GetWalletNfts, NFT } from 'types';
import { getNft } from './getNFT';
import { getWalletTokens } from './getWalletTokens';

export const getWalletNfts: GetWalletNfts = async (publicKey, connection) => {
  const tokens = await getWalletTokens(publicKey, connection);

  const magnumNftTokens = tokens.filter(
    token => hashes.includes(token.publicKey.toString()) && Number(token.balance) > 0
  );

  const collection = (await Promise.all(
    magnumNftTokens.map(async token => {
      const nft = await getNft(connection, token.publicKey, token.tokenAccount);
      return nft;
    })
  )) as NFT[];

  return collection;
};
