import { deprecated } from '@metaplex-foundation/mpl-token-metadata';
import axios from 'axios';

import { GetMetaDataByMint } from 'types/types.nft';

export const getMetaDataByMint: GetMetaDataByMint = async (connection, mint) => {
  const { data, info } = await deprecated.Metadata.findByMint(connection, mint);

  const metadata = await axios.get(data?.data.uri as string);

  return { data, info, metadata };
};
