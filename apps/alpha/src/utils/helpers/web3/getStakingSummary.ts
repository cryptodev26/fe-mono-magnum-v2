import { GetStakingSummary, SummaryAccount } from 'types';
import { getPda } from './getPda';

export const getStakingSummary: GetStakingSummary = async program => {
  const {
    account: { summaryAccount: summary },
  } = program;

  const [summaryPda] = await getPda(program.programId, 'summary2', []);

  const { currentStaked, stakeHistory, totalCollected } = await summary.fetch(summaryPda);

  const summaryAccount: SummaryAccount = {
    currentStaked: currentStaked.toNumber(),
    stakeHistory: stakeHistory.toNumber(),
    totalCollected: totalCollected.toNumber(),
  };

  return { summaryAccount, summaryPda };
};
