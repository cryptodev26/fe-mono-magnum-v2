import { TOKEN_PROGRAM_ID } from '@project-serum/anchor/dist/cjs/utils/token';
import { PublicKey } from '@solana/web3.js';
import { GetWalletTokens, Token } from 'types';

export const getWalletTokens: GetWalletTokens = async (publicKey, connection) => {
  try {
    const tokenAccounts = await connection.getParsedTokenAccountsByOwner(publicKey, {
      programId: TOKEN_PROGRAM_ID,
    });

    const tokens: Token[] = tokenAccounts.value.map(tokenAccount => ({
      publicKey: new PublicKey(tokenAccount.account.data.parsed.info.mint),
      tokenAccount: new PublicKey(tokenAccount.pubkey),
      balance: tokenAccount.account.data.parsed.info.tokenAmount.amount,
    }));

    return tokens;
  } catch (err) {
    console.error(`error getting tokens: `, err);
    return [];
  }
};
