import { utils } from '@project-serum/anchor';
import { PublicKey } from '@solana/web3.js';

import { GetPda } from 'types';

export const getPda: GetPda = (programId, arg, keys) => {
  const encode = Buffer.from(utils.bytes.utf8.encode(arg));
  const buffers = keys.map(key => key.toBuffer());
  const completeArgs = [encode, ...buffers];

  return PublicKey.findProgramAddress(completeArgs, programId);
};
