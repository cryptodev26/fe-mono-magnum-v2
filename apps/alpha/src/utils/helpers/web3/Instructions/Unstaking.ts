import { TOKEN_PROGRAM_ID } from '@project-serum/anchor/dist/cjs/utils/token';
import { SystemProgram, SYSVAR_RENT_PUBKEY, PublicKey } from '@solana/web3.js';

import { CreateUnstakingInstructions } from 'types';

import { getPda } from '../getPda';
import { getStakingSummary } from '../getStakingSummary';

const SPL_ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID: PublicKey = new PublicKey(
  'ATokenGPvbdGVxr1b2hvZbsiqW5xWH25efTNsLJA8knL'
);

export const createUnstakingInstructions: CreateUnstakingInstructions = async (
  program,
  nft
) => {
  const { stakedAccount, stakedAccountPublicKey } = nft;

  if (!stakedAccount || !stakedAccountPublicKey)
    throw new Error('no staking account found');

  const [vaultAccount] = await getPda(program.programId, 'receipt', [
    stakedAccountPublicKey,
    nft.stakedAccount?.stakingMint as PublicKey,
  ]);

  const [vaultAuthority] = await getPda(program.programId, 'vault', [
    stakedAccountPublicKey,
    stakedAccount.stakingMint,
  ]);

  const { summaryPda } = await getStakingSummary(program);

  const sender = program.provider.publicKey;

  return {
    accounts: {
      stakingTokenOwner: sender as PublicKey,
      stakingMint: stakedAccount.stakingMint, // variable - escrowItem.mint
      ownerStakingTokenAccount: stakedAccount.ownerStakingTokenAccount, // variable | escrowItem.initializer_deposit_token_account
      vaultAccount, // variable | above
      vaultAuthority, // variable | above
      stakingAccount: stakedAccountPublicKey, // variable | escrowItem.escrow_pk
      summaryAccount: summaryPda,
      tokenProgram: TOKEN_PROGRAM_ID,
      systemProgram: SystemProgram.programId,
      rent: SYSVAR_RENT_PUBKEY,
      associatedTokenProgram: SPL_ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID,
    },
    fallbackTx: null,
  };
};
