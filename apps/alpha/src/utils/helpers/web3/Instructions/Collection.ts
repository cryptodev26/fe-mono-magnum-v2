import { PublicKey, SystemProgram, SYSVAR_RENT_PUBKEY } from '@solana/web3.js';
import { TOKEN_PROGRAM_ID } from '@project-serum/anchor/dist/cjs/utils/token';

import { CreateCollectionInstructions, StakedTokenAccount } from 'types';

import { getPda } from '../getPda';
import { getStakingSummary } from '../getStakingSummary';
import { getATA } from '../getATA';

const SPL_ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID: PublicKey = new PublicKey(
  'ATokenGPvbdGVxr1b2hvZbsiqW5xWH25efTNsLJA8knL'
);

export const createCollectionInstructions: CreateCollectionInstructions = async (
  provider,
  program,
  nft
) => {
  const { stakedAccount, stakedAccountPublicKey } = nft;

  const rewardMintPublicKey = new PublicKey(
    'MAGf4MnUUkkAUUdiYbNFcDnE4EBGHJYLk9foJ2ae7BV'
  );

  const [mintAuthority] = await getPda(program.programId, 'authority', [
    rewardMintPublicKey,
  ]);
  const { summaryPda } = await getStakingSummary(program);
  const { ata, transaction } = await getATA(rewardMintPublicKey, provider);

  return {
    accounts: {
      rewardMintAuthority: mintAuthority,
      stakingTokenOwner: provider.publicKey,
      ownerStakingTokenAccount: (stakedAccount as StakedTokenAccount)
        .ownerStakingTokenAccount,
      stakingAccount: stakedAccountPublicKey as PublicKey,
      stakingMint: (stakedAccount as StakedTokenAccount).stakingMint,
      rewardMint: rewardMintPublicKey,
      ownerRewardTokenAccount: ata,
      summaryAccount: summaryPda,
      tokenProgram: TOKEN_PROGRAM_ID,
      systemProgram: SystemProgram.programId,
      rent: SYSVAR_RENT_PUBKEY,
      associatedTokenProgram: SPL_ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID,
    },
    fallbackTx: transaction,
  };
};
