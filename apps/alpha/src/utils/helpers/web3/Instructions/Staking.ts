import { Keypair, SystemProgram, SYSVAR_RENT_PUBKEY } from '@solana/web3.js';
import { TOKEN_PROGRAM_ID } from '@project-serum/anchor/dist/cjs/utils/token';

import { CreateStakingInstructions, PublicKey } from 'types';

import { getPda } from '../getPda';
import { getStakingSummary } from '../getStakingSummary';
import { getATA } from '../getATA';

export const createStakingInstructions: CreateStakingInstructions = async (
  provider,
  program,
  nft,
  lockup
) => {
  const escrowKeypair = Keypair.generate();
  const stakingTokenOwner = provider.publicKey;
  const { ata, transaction } = await getATA(nft.mint, provider);
  const { summaryPda } = await getStakingSummary(program);
  const [vaultAccount] = await getPda(program.programId, 'receipt', [
    escrowKeypair.publicKey,
    nft.mint,
  ]);

  return {
    stake: {
      lockup,
      legendary: nft.isLegendary,
    },
    accounts: {
      stakingTokenOwner,
      stakingMint: nft.mint,
      vaultAccount,
      ownerStakingTokenAccount: nft.assTokenAccount as PublicKey,
      ownerRewardTokenAccount: ata,
      stakingAccount: escrowKeypair.publicKey,
      summaryAccount: summaryPda,
      systemProgram: SystemProgram.programId,
      rent: SYSVAR_RENT_PUBKEY,
      tokenProgram: TOKEN_PROGRAM_ID,
    },
    signers: [
      escrowKeypair,
      {
        publicKey: stakingTokenOwner,
        secretKey: stakingTokenOwner.toBytes(),
      },
    ],
    fallbackTx: transaction,
  };
};
