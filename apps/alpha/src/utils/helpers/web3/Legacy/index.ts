/* eslint-disable @typescript-eslint/ban-ts-comment */
import { Provider, AnchorProvider } from '@project-serum/anchor';
import { PublicKey, SystemProgram, SYSVAR_RENT_PUBKEY } from '@solana/web3.js';
import { TOKEN_PROGRAM_ID } from '@solana/spl-token';

import { NFT, StakingProgram } from 'types';

import { getPda } from '../getPda';
import { getATA } from '../getATA';

const SPL_ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID: PublicKey = new PublicKey(
  'ATokenGPvbdGVxr1b2hvZbsiqW5xWH25efTNsLJA8knL'
);

const rewardMint = 'MAGf4MnUUkkAUUdiYbNFcDnE4EBGHJYLk9foJ2ae7BV';
const rewardMintPk = new PublicKey(rewardMint);

const errorHandler = async (provider: Provider, transactionId: string) => {
  const confirmation = await provider.connection.confirmTransaction(transactionId);
  if (!confirmation) throw new Error('Timed out awaiting confirmation on transaction');

  if (confirmation.value.err) {
    // TODO: Maybe here we can add a toast for the user
    // const transaction = await provider.connection.getTransaction(transactionId);

    throw new Error('Transaction failed: Custom instruction error');
  }
};

const collectTokenRewardsLegacy = async (
  provider: AnchorProvider,
  program: StakingProgram,
  token: NFT
) => {
  if (!token.stakedAccount || !token.stakedAccountPublicKey) return;
  const { ata } = await getATA(rewardMintPk, provider);

  await getATA(token.mint, provider);

  try {
    // FOR V2:
    // Devnet: C3awfvUTqPh6LAWg1Ts9wb2qQaEVe1VsAJxwXVtno27x

    // @ts-expect-error
    const walletPk = program.provider.wallet;

    if (!walletPk) return;

    const [rewardMintAuthority] = await getPda(program.programId, 'authority', [
      rewardMintPk,
    ]);

    const tx = await program.rpc.collectFull({
      accounts: {
        rewardMintAuthority,
        stakingTokenOwner: walletPk.publicKey,
        ownerStakingTokenAccount: token.stakedAccount.ownerStakingTokenAccount,
        stakingAccount: token.stakedAccountPublicKey,
        stakingMint: token.stakedAccount.stakingMint,
        rewardMint: rewardMintPk,
        ownerRewardTokenAccount: ata,
        tokenProgram: TOKEN_PROGRAM_ID,
      },
      signers: [],
    });

    await errorHandler(provider, tx);

    // TODO: Add Toast
    const message = 'success';
    return message;
  } catch (err) {
    console.error(err);
  }
};

const findProgramsUnstaking = async (
  programId: PublicKey,
  stakedAccountPubKey: PublicKey,
  stakingMintPubKey: PublicKey
) => {
  const [vaultAccount] = await getPda(programId, 'receipt', [
    stakedAccountPubKey,
    stakingMintPubKey,
  ]);

  const [summaryAccount] = await getPda(programId, 'summary2', []);

  const [vaultAuthority] = await getPda(programId, 'vault', [
    stakedAccountPubKey,
    stakingMintPubKey,
  ]);

  return { vaultAccount, summaryAccount, vaultAuthority };
};

export const unstakeLegacy = async (
  provider: AnchorProvider,
  program: StakingProgram,
  stakedToken: NFT
) => {
  const { stakedAccountPublicKey, stakedAccount } = stakedToken;

  if (!stakedAccountPublicKey || !stakedAccount) return;

  if (!stakedAccount.rewardCollected) {
    const collection = await collectTokenRewardsLegacy(provider, program, stakedToken);
    if (!collection) return;
  }

  try {
    const { summaryAccount, vaultAccount, vaultAuthority } = await findProgramsUnstaking(
      program.programId,
      stakedAccountPublicKey,
      stakedAccount.stakingMint
    );

    // @ts-expect-error
    const sender = program.provider.wallet;

    if (!sender) return;

    const tx = await program.transaction.unstake({
      accounts: {
        stakingTokenOwner: sender.publicKey,
        stakingMint: stakedAccount.stakingMint,
        ownerStakingTokenAccount: stakedAccount.ownerStakingTokenAccount,
        vaultAccount,
        vaultAuthority,
        stakingAccount: stakedToken.stakedAccountPublicKey as PublicKey,
        summaryAccount,
        tokenProgram: TOKEN_PROGRAM_ID,
        // @ts-ignore
        systemProgram: SystemProgram.programId,
        rent: SYSVAR_RENT_PUBKEY,
        associatedTokenProgram: SPL_ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID,
      },
      signers: [sender],
    });

    tx.feePayer = await sender.publicKey;
    const blockhashObj = await provider.connection.getRecentBlockhash();
    tx.recentBlockhash = await blockhashObj.blockhash;

    const signedTransaction = await sender.signTransaction(tx);

    const transactionId = await provider.connection.sendRawTransaction(
      signedTransaction.serialize()
    );

    return { transaction: transactionId, error: undefined };
  } catch (err) {
    return { transaction: undefined, error: err };
  }
};
