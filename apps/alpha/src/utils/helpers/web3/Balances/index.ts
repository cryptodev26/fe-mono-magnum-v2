import { LAMPORTS_PER_SOL } from '@solana/web3.js';
import { GetUserMagBalance, GetUserSOLBalance } from 'types';
import { getWalletTokens } from '../getWalletTokens';

export const getUserMagBalance: GetUserMagBalance = async (publicKey, connection) => {
  const tokens = await getWalletTokens(publicKey, connection);

  return tokens.find(
    token => token.publicKey.toString() === 'MAGf4MnUUkkAUUdiYbNFcDnE4EBGHJYLk9foJ2ae7BV'
  )?.balance as number;
};

export const getUserSolBalance: GetUserSOLBalance = async (publicKey, connection) => {
  let solBalance = await connection.getBalance(publicKey, 'confirmed');
  solBalance /= LAMPORTS_PER_SOL;

  return solBalance;
};
