import { GetUserMagBalance } from 'types';
import { getWalletTokens } from '../getWalletTokens';

export const getUserMagBalance: GetUserMagBalance = async (publicKey, connection) => {
  const tokens = await getWalletTokens(publicKey, connection);

  return tokens.find(
    token => token.publicKey.toString() === 'MAGf4MnUUkkAUUdiYbNFcDnE4EBGHJYLk9foJ2ae7BV'
  )?.balance as number;
};
