import { LAMPORTS_PER_SOL } from '@solana/web3.js';

import { GetUserSOLBalance } from 'types';

export const getUserSolBalance: GetUserSOLBalance = async (publicKey, connection) => {
  let solBalance = await connection.getBalance(publicKey, 'confirmed');
  solBalance /= LAMPORTS_PER_SOL;

  return solBalance;
};
