import { GetStakedNfts, NFT, StakedTokenAccount } from 'types';
import { getNft } from './getNFT';

export const getStakedNfts: GetStakedNfts = async (publicKey, connection, program) => {
  const allStakedTokens = await program.account.stakeAccount.all();
  if (allStakedTokens.length < 1)
    throw new Error('error getting tokens from staking program');

  const usersStakedTokens = allStakedTokens.filter(
    token => token.account.stakingTokenOwner.toString() === publicKey.toString()
  );

  if (usersStakedTokens.length < 1) return [];

  const collection = (await Promise.all(
    usersStakedTokens.map(async token => {
      const nft = await getNft(
        connection,
        token.account.stakingMint,
        token.account.ownerStakingTokenAccount,
        token.account as StakedTokenAccount,
        token.publicKey
      );
      return nft;
    })
  )) as NFT[];

  return collection;
};
