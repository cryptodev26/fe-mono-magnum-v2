import {
  /**
   * @solana/spl-token exists as its own package: node_modules/@solana/spl-token
   * and as a dependency of @metaplex-foundation/mpl-core
   * getAssociatedTokenAddress and createAssociatedTokenAccountInstruction are exported from the @solana/spl-token
   * but not from the dependency of @metaplex-foundation/mpll-core
   * which throws error ts(2305)
   */
  // @ts-expect-error: see comment at top of file
  getAssociatedTokenAddress,
  // @ts-expect-error: see comment at top of file
  createAssociatedTokenAccountInstruction,
} from '@solana/spl-token';
import { Transaction } from '@solana/web3.js';

import { GetATA, PublicKey } from 'types';

export const getATA: GetATA = async (publicKey, provider) => {
  const { connection, wallet } = provider;

  const { value } = await connection.getParsedTokenAccountsByOwner(
    provider.publicKey as PublicKey,
    {
      mint: publicKey,
    }
  );

  // const { value } = associatedTokenAccounts;

  if (!value || value.length === 0) {
    const fallBackAssociatedTokenAccounts = await getAssociatedTokenAddress(
      publicKey,
      provider.publicKey as PublicKey
    );

    const fallBackTransaction = new Transaction().add(
      createAssociatedTokenAccountInstruction(
        wallet.publicKey,
        fallBackAssociatedTokenAccounts,
        wallet.publicKey,
        publicKey
      )
    );

    fallBackTransaction.feePayer = wallet.publicKey;
    fallBackTransaction.recentBlockhash = (
      await provider.connection.getLatestBlockhash()
    ).blockhash;

    const signedTransaction = await wallet.signTransaction(fallBackTransaction);
    const serializedSignature = signedTransaction.serialize();
    await provider.connection.sendRawTransaction(serializedSignature);

    return { ata: fallBackAssociatedTokenAccounts, transaction: fallBackTransaction };
  }

  return { ata: value[0].pubkey, transaction: null };
};
