import { AnchorProvider } from '@project-serum/anchor';

import { ProviderInit, PublicKey, Transaction } from 'types';

export const initProvider: ProviderInit = (connection, wallet) =>
  new Promise(resolve => {
    const { signTransaction, signAllTransactions } = wallet;

    const provider = new AnchorProvider(
      connection,
      {
        signTransaction: signTransaction as (tx: Transaction) => Promise<Transaction>,
        signAllTransactions: signAllTransactions as (
          txs: Transaction[]
        ) => Promise<Transaction[]>,
        publicKey: wallet.publicKey as PublicKey,
      },
      {
        preflightCommitment: 'processed',
        commitment: 'confirmed',
      }
    );

    resolve(provider);
  });
