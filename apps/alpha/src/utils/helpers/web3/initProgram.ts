import { Program } from '@project-serum/anchor';
import { PublicKey } from '@solana/web3.js';

import { IDL } from 'idl/staking';
import { ProgramInit } from 'types';

export const initProgram: ProgramInit = provider =>
  new Promise(resolve => {
    const stakingProgramId = new PublicKey(
      'HeQTtHWw83MhXGQXgWdQdFKbwhRuNDyRcTo648WsXHnT'
    );

    const program = new Program(IDL, stakingProgramId, provider);

    resolve(program);
  });
