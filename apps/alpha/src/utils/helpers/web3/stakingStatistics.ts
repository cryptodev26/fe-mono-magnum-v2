import dayjs from 'dayjs';

import { NFT } from 'types';

const dailyCalculation = (nft: NFT, value: number) => {
  if (!nft.stakedAccount) return 0;

  if (!nft.stakedAccount?.isV2) {
    const lockup = nft.stakedAccount?.stakingPeriod;

    if (lockup === 0) return value * 7;
    if (lockup === 1) return value * 14;
    if (lockup === 2) return value * 28;
  }

  const now = dayjs();
  const stakeDate = dayjs.unix(Number(nft.stakedAccount?.created.toString()));
  const secondsSinceStake = Math.round(now.diff(stakeDate) / 1000);

  const reward = Number(secondsSinceStake * (value / 24 / 60 / 60));

  // console.log({
  //   now,
  //   stakeDate,
  //   secondsSinceStake,
  //   reward,
  // });

  return reward;
};

export const estimatedTotalRewards = (stakedNFTs?: NFT[]) => {
  if (!stakedNFTs) return 0;

  return stakedNFTs.reduce((prev, current) => {
    if (!current.stakedAccount) return prev;

    if (current.stakedAccount.stakingPeriod === 0) {
      const estimatedReward = dailyCalculation(current, current.isLegendary ? 7 : 5);
      // console.log(0, { prev, current, estimatedReward });

      return prev + estimatedReward;
    }

    if (current.stakedAccount.stakingPeriod === 1) {
      const estimatedReward = dailyCalculation(current, current.isLegendary ? 10 : 7);
      // console.log(1, { prev, current, estimatedReward });
      return prev + estimatedReward;
    }

    if (current.stakedAccount.stakingPeriod === 2) {
      const estimatedReward = dailyCalculation(current, current.isLegendary ? 15 : 10);
      // console.log(2, { prev, current, estimatedReward });
      return prev + estimatedReward;
    }

    return prev;
  }, 0);
};

export const estimatedDailyRewards = (stakedNFTs?: NFT[]) => {
  if (!stakedNFTs) return 0;

  return stakedNFTs.reduce((prev, current) => {
    if (!current.stakedAccount) return prev;

    if (current.stakedAccount.stakingPeriod === 0) {
      const value = current.isLegendary ? 7 : 5;
      const daily = Math.round((value * 100) / 100);

      return prev + daily;
    }

    if (current.stakedAccount.stakingPeriod === 1) {
      const value = current.isLegendary ? 10 : 7;
      const daily = Math.round(value * 100) / 100;

      return prev + daily;
    }

    if (current.stakedAccount.stakingPeriod === 2) {
      const value = current.isLegendary ? 15 : 10;
      const daily = Math.round(value * 100) / 100;

      return prev + daily;
    }

    return prev;
  }, 0);
};
