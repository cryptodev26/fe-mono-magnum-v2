export type Staking = {
  version: '0.1.0';
  name: 'staking';
  instructions: [
    {
      name: 'initMintAuthority';
      accounts: [
        {
          name: 'mintAuthority';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'rewardMint';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'admin';
          isMut: true;
          isSigner: true;
        },
        {
          name: 'systemProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'rent';
          isMut: false;
          isSigner: false;
        }
      ];
      args: [];
    },
    {
      name: 'stake';
      accounts: [
        {
          name: 'stakingTokenOwner';
          isMut: true;
          isSigner: true;
        },
        {
          name: 'stakingMint';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'vaultAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'ownerStakingTokenAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'ownerRewardTokenAccount';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'stakingAccount';
          isMut: true;
          isSigner: true;
        },
        {
          name: 'summaryAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'systemProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'rent';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'tokenProgram';
          isMut: false;
          isSigner: false;
        }
      ];
      args: [
        {
          name: 'stakingPeriod';
          type: 'u16';
        },
        {
          name: 'isOneOfOne';
          type: 'bool';
        }
      ];
    },
    {
      name: 'collect';
      accounts: [
        {
          name: 'rewardMintAuthority';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'stakingTokenOwner';
          isMut: true;
          isSigner: true;
        },
        {
          name: 'ownerStakingTokenAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'stakingAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'stakingMint';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'rewardMint';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'ownerRewardTokenAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'summaryAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'tokenProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'systemProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'associatedTokenProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'rent';
          isMut: false;
          isSigner: false;
        }
      ];
      args: [];
    },
    {
      name: 'collectFull';
      accounts: [
        {
          name: 'rewardMintAuthority';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'stakingTokenOwner';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'ownerStakingTokenAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'stakingAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'stakingMint';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'rewardMint';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'ownerRewardTokenAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'tokenProgram';
          isMut: false;
          isSigner: false;
        }
      ];
      args: [];
    },
    {
      name: 'unstake';
      accounts: [
        {
          name: 'stakingTokenOwner';
          isMut: true;
          isSigner: true;
        },
        {
          name: 'stakingMint';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'vaultAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'vaultAuthority';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'ownerStakingTokenAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'stakingAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'summaryAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'tokenProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'systemProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'associatedTokenProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'rent';
          isMut: false;
          isSigner: false;
        }
      ];
      args: [];
    },
    {
      name: 'initSummary';
      accounts: [
        {
          name: 'owner';
          isMut: true;
          isSigner: true;
        },
        {
          name: 'summaryAccount';
          isMut: true;
          isSigner: false;
        },
        {
          name: 'systemProgram';
          isMut: false;
          isSigner: false;
        },
        {
          name: 'rent';
          isMut: false;
          isSigner: false;
        }
      ];
      args: [];
    }
  ];
  accounts: [
    {
      name: 'stakeAccount';
      type: {
        kind: 'struct';
        fields: [
          {
            name: 'stakingTokenOwner';
            type: 'publicKey';
          },
          {
            name: 'ownerStakingTokenAccount';
            type: 'publicKey';
          },
          {
            name: 'stakingMint';
            type: 'publicKey';
          },
          {
            name: 'created';
            type: 'i64';
          },
          {
            name: 'unstakeDate';
            type: 'i64';
          },
          {
            name: 'stakingPeriod';
            type: 'u16';
          },
          {
            name: 'isOneOfOne';
            type: 'bool';
          },
          {
            name: 'rewardCollected';
            type: 'bool';
          },
          {
            name: 'lastRewardCollection';
            type: 'i64';
          },
          {
            name: 'totalRewardCollected';
            type: 'i64';
          },
          {
            name: 'ownerRewardTokenAccount';
            type: 'publicKey';
          },
          {
            name: 'isV2';
            type: 'bool';
          }
        ];
      };
    },
    {
      name: 'summaryAccount';
      type: {
        kind: 'struct';
        fields: [
          {
            name: 'stakeHistory';
            type: 'u64';
          },
          {
            name: 'currentStaked';
            type: 'u64';
          },
          {
            name: 'totalCollected';
            type: 'i64';
          }
        ];
      };
    }
  ];
  errors: [
    {
      code: 6000;
      name: 'NotEnoughElapsedSinceLastCollection';
      msg: 'Not enough time has elapsed since your last collection.';
    },
    {
      code: 6001;
      name: 'TooEarlyToUnstake';
      msg: 'It is too early to unstaked this token.';
    },
    {
      code: 6002;
      name: 'FullRewardNotCollected';
      msg: 'The reward has not been collected. Something must have gone wrong.';
    },
    {
      code: 6003;
      name: 'FullRewardAlreadyCollected';
      msg: 'The reward has already been collected for this staking period.';
    },
    {
      code: 6004;
      name: 'InvalidStakingPeriod';
      msg: 'The staking period is not valid.';
    },
    {
      code: 6005;
      name: 'NotAV2StakingAccounbt';
      msg: 'This is not a V2 staking account.';
    },
    {
      code: 6006;
      name: 'V2StakingAccounbt';
      msg: 'This is a V2 staking account.';
    }
  ];
};

export const IDL: Staking = {
  version: '0.1.0',
  name: 'staking',
  instructions: [
    {
      name: 'initMintAuthority',
      accounts: [
        {
          name: 'mintAuthority',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'rewardMint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'admin',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [],
    },
    {
      name: 'stake',
      accounts: [
        {
          name: 'stakingTokenOwner',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'stakingMint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'vaultAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'ownerStakingTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'ownerRewardTokenAccount',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'stakingAccount',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'summaryAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'stakingPeriod',
          type: 'u16',
        },
        {
          name: 'isOneOfOne',
          type: 'bool',
        },
      ],
    },
    {
      name: 'collect',
      accounts: [
        {
          name: 'rewardMintAuthority',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'stakingTokenOwner',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'ownerStakingTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'stakingAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'stakingMint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rewardMint',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'ownerRewardTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'summaryAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'associatedTokenProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [],
    },
    {
      name: 'collectFull',
      accounts: [
        {
          name: 'rewardMintAuthority',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'stakingTokenOwner',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'ownerStakingTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'stakingAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'stakingMint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rewardMint',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'ownerRewardTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [],
    },
    {
      name: 'unstake',
      accounts: [
        {
          name: 'stakingTokenOwner',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'stakingMint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'vaultAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'vaultAuthority',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'ownerStakingTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'stakingAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'summaryAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'associatedTokenProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [],
    },
    {
      name: 'initSummary',
      accounts: [
        {
          name: 'owner',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'summaryAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [],
    },
  ],
  accounts: [
    {
      name: 'stakeAccount',
      type: {
        kind: 'struct',
        fields: [
          {
            name: 'stakingTokenOwner',
            type: 'publicKey',
          },
          {
            name: 'ownerStakingTokenAccount',
            type: 'publicKey',
          },
          {
            name: 'stakingMint',
            type: 'publicKey',
          },
          {
            name: 'created',
            type: 'i64',
          },
          {
            name: 'unstakeDate',
            type: 'i64',
          },
          {
            name: 'stakingPeriod',
            type: 'u16',
          },
          {
            name: 'isOneOfOne',
            type: 'bool',
          },
          {
            name: 'rewardCollected',
            type: 'bool',
          },
          {
            name: 'lastRewardCollection',
            type: 'i64',
          },
          {
            name: 'totalRewardCollected',
            type: 'i64',
          },
          {
            name: 'ownerRewardTokenAccount',
            type: 'publicKey',
          },
          {
            name: 'isV2',
            type: 'bool',
          },
        ],
      },
    },
    {
      name: 'summaryAccount',
      type: {
        kind: 'struct',
        fields: [
          {
            name: 'stakeHistory',
            type: 'u64',
          },
          {
            name: 'currentStaked',
            type: 'u64',
          },
          {
            name: 'totalCollected',
            type: 'i64',
          },
        ],
      },
    },
  ],
  errors: [
    {
      code: 6000,
      name: 'NotEnoughElapsedSinceLastCollection',
      msg: 'Not enough time has elapsed since your last collection.',
    },
    {
      code: 6001,
      name: 'TooEarlyToUnstake',
      msg: 'It is too early to unstaked this token.',
    },
    {
      code: 6002,
      name: 'FullRewardNotCollected',
      msg: 'The reward has not been collected. Something must have gone wrong.',
    },
    {
      code: 6003,
      name: 'FullRewardAlreadyCollected',
      msg: 'The reward has already been collected for this staking period.',
    },
    {
      code: 6004,
      name: 'InvalidStakingPeriod',
      msg: 'The staking period is not valid.',
    },
    {
      code: 6005,
      name: 'NotAV2StakingAccounbt',
      msg: 'This is not a V2 staking account.',
    },
    {
      code: 6006,
      name: 'V2StakingAccounbt',
      msg: 'This is a V2 staking account.',
    },
  ],
};
