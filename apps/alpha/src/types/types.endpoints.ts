type EndpointTypes = 'mainnet' | 'devnet' | 'localnet';

export type { EndpointTypes };
