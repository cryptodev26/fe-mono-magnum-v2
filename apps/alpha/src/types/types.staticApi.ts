import { AxiosError } from 'axios';

interface StaticAttribute {
  img: string;
  rating: number;
  trait_type: string;
  value: string;
  __v: number;
  _id: string;
}

interface StaticAttributes {
  loading: boolean;
  error?: AxiosError;
  data: {
    background: StaticAttribute[];
    body: StaticAttribute[];
    costume: StaticAttribute[];
    face_gear: StaticAttribute[];
    face: StaticAttribute[];
    glasses: StaticAttribute[];
    headgear: StaticAttribute[];
  };
}

interface GenesisCollection {
  attributes: StaticAttributes['data'];
}

interface StaticApiContextState {
  data: {
    genesis: GenesisCollection;
  };
}

export type { StaticAttributes, StaticApiContextState };
