import {
  Connection,
  PublicKey,
  StakingProvider,
  StakingProgram,
  SummaryAccount,
  Transaction,
  WalletContextState,
  RpcResponseAndContext,
  SignatureResult,
  Keypair,
} from './types.universal';
import { NFT } from './types.nft';

interface StakingSummary {
  summaryAccount: SummaryAccount;
  summaryPda: PublicKey;
}

interface ATA {
  ata: PublicKey;
  transaction: Transaction | null;
}

type ProviderInit = (
  connection: Connection,
  wallet: WalletContextState
) => Promise<StakingProvider>;

type ProgramInit = (provider: StakingProvider) => Promise<StakingProgram>;

type TransactionInit = (
  program: StakingProgram,
  wallet: WalletContextState
) => Promise<Transaction>;

type GetStakingSummary = (program: StakingProgram) => Promise<StakingSummary>;

type GetPda = (
  programId: PublicKey,
  arg: string,
  buffers: PublicKey[]
) => Promise<[PublicKey, number]>;

type GetATA = (publicKey: PublicKey, provider: StakingProvider) => Promise<ATA>;

interface BaseTransactionAccount {
  stakingTokenOwner: PublicKey;
  stakingMint: PublicKey;
  ownerStakingTokenAccount: PublicKey;
  stakingAccount: PublicKey;
  summaryAccount: PublicKey;
  tokenProgram: PublicKey;
}

interface StakeTransactionAccounts extends BaseTransactionAccount {
  vaultAccount: PublicKey;
  ownerRewardTokenAccount: PublicKey;
  systemProgram: PublicKey;
  rent: PublicKey;
}

interface CollectTransactionAccounts extends BaseTransactionAccount {
  rewardMintAuthority: PublicKey;
  rewardMint: PublicKey;
  ownerRewardTokenAccount: PublicKey;
}

interface UnstakeTransactionAccounts extends BaseTransactionAccount {
  vaultAccount: PublicKey;
  vaultAuthority: PublicKey;
}

interface StakingInstructions {
  stake: {
    lockup: number;
    legendary: boolean;
  };
  accounts: StakeTransactionAccounts;
  signers: (
    | Keypair
    | {
        publicKey: PublicKey;
        secretKey: Uint8Array;
      }
  )[];
  fallbackTx: Transaction | null;
}

interface CollectionInstructions {
  accounts: CollectTransactionAccounts;
  fallbackTx: Transaction | null;
}

interface UnstakingInstructions {
  accounts: UnstakeTransactionAccounts;
  fallbackTx: Transaction | null;
}

type CreateStakingInstructions = (
  provider: StakingProvider,
  program: StakingProgram,
  nft: NFT,
  lockup: number
) => Promise<StakingInstructions>;

type CreateUnstakingInstructions = (
  program: StakingProgram,
  nft: NFT
) => Promise<UnstakingInstructions>;

type CreateCollectionInstructions = (
  provider: StakingProvider,
  program: StakingProgram,
  nft: NFT
) => Promise<CollectionInstructions>;

type TransactionType = 'stake' | 'unstake' | 'collect';

type UseStakingTransaction = () => {
  stakingTransaction: (
    actionType: TransactionType,
    nfts: NFT[],
    lockup?: number
  ) => Promise<Error | undefined>;
  loading: boolean;
};

type TransactionHook = (
  stakingProvider: StakingProvider,
  stakingProgram: StakingProgram,
  wallet: WalletContextState
) => (
  nfts: NFT[],
  lockup?: number
) => Promise<
  | {
      transaction: Transaction;
      signers?: Keypair[];
      error: Error | undefined;
    }
  | undefined
>;

type SignTransactionHook = (
  wallet: WalletContextState,
  connection: Connection
) => (
  transaction: Transaction,
  signers?: Keypair[]
) => Promise<Error | RpcResponseAndContext<SignatureResult> | undefined>;

export type {
  CreateStakingInstructions,
  CreateCollectionInstructions,
  CreateUnstakingInstructions,
  ProviderInit,
  ProgramInit,
  TransactionHook,
  TransactionInit,
  TransactionType,
  UseStakingTransaction,
  GetATA,
  GetPda,
  GetStakingSummary,
  SignTransactionHook,
  //
  Connection,
  PublicKey,
  StakingProvider,
  StakingProgram,
  SummaryAccount,
  Transaction,
  WalletContextState,
  RpcResponseAndContext,
  SignatureResult,
  Keypair,
};
