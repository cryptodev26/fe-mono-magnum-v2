import {
  Connection,
  PublicKey,
  StakingProgram,
  WalletContextState,
} from './types.universal';
import { NFT } from './types.nft';

type GetUserSOLBalance = (
  publicKey: PublicKey,
  connection: Connection
) => Promise<number>;

type GetUserMagBalance = (
  publicKey: PublicKey,
  connection: Connection
) => Promise<number>;

type GetWalletNfts = (publicKey: PublicKey, connection: Connection) => Promise<NFT[]>;

type GetStakedNfts = (
  publicKey: PublicKey,
  connection: Connection,
  program: StakingProgram
) => Promise<NFT[]>;

interface User {
  userWallet: WalletContextState;
  connection: Connection;
  balances: UserBalances;
  nfts: UserNFTs;
  _proto_: UserProto;
}

interface UserBalances {
  sol: number;
  mag: number;
  getBalance(publicKey: PublicKey, connection: Connection, token: 'SOL' | 'MAG'): void;
}

interface UserNFTs {
  wallet: NFT[] | [];
  staked: NFT[] | [];
  getNfts(
    publicKey: PublicKey,
    connection: Connection,
    from: 'wallet' | 'staked',
    program?: StakingProgram
  ): void;
}

interface UserProto {
  setWallet(wallet: WalletContextState): void;
  setConnection(connection: Connection): void;
}

export type {
  GetUserMagBalance,
  GetStakedNfts,
  GetUserSOLBalance,
  GetWalletNfts,
  User,
  UserBalances,
  UserNFTs,
  UserProto,
};
