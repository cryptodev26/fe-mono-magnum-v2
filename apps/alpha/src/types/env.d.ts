declare global {
  namespace NodeJS {
    interface ProcessEnv {
      API_ORIGIN: string;
      API_KEY: string;
      ENV: 'test' | 'dev' | 'prod';
    }
  }
}
