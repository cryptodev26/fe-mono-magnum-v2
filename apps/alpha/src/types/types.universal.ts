import { AnchorProvider, BN, Program } from '@project-serum/anchor';
import { deprecated } from '@metaplex-foundation/mpl-token-metadata';
import { WalletContextState } from '@solana/wallet-adapter-react';
import {
  AccountInfo,
  Connection,
  Keypair,
  PublicKey,
  Transaction,
  RpcResponseAndContext,
  SignatureResult,
} from '@solana/web3.js';

import { Staking } from 'idl/staking'; // set path not working here

type MetadataData = deprecated.MetadataData;

type StakingProgram = Program<Staking>;
type StakingProvider = AnchorProvider;

type SummaryAccount = {
  currentStaked: number;
  stakeHistory: number;
  totalCollected: number;
};

export type {
  AccountInfo, // imported
  BN, // imported
  Connection, // imported
  Keypair, // imported
  MetadataData, // imported
  PublicKey, // imported
  RpcResponseAndContext, // imported
  SignatureResult, // imported
  StakingProgram, // imported
  StakingProvider, // imported
  SummaryAccount, // imported
  Transaction, // imported
  WalletContextState, // imported
};
