import { AxiosResponse } from 'axios';
import { AccountInfo, BN, Connection, MetadataData, PublicKey } from './types.universal';

interface Metadata {
  data: MetadataData | undefined;
  info: AccountInfo<Buffer> | undefined;
  metadata: AxiosResponse<FetchedMetaData>;
}

interface FetchedMetaData {
  attributes: Attribute[];
  collection: {
    family: string;
    name: string;
  };
  description: string;
  external_url: string;
  image: string;
  properties: {
    category: string;
    creators: { address: string; share: number }[];
    files: { type: string; uri: string }[];
  };
  seller_fee_basis_points: number;
  symbol: string;
  name: string;
}

interface StakedTokenAccount {
  created: BN;
  isOneOfOne: boolean;
  isV2: boolean;
  lastRewardCollection: BN;
  ownerRewardTokenAccount: PublicKey;
  ownerStakingTokenAccount: PublicKey;
  rewardCollected: boolean;
  stakingMint: PublicKey;
  stakingPeriod: number;
  stakingTokenOwner: PublicKey;
  totalRewardCollected: BN;
  unstakeDate: BN;
}

type GetMetaDataByMint = (connection: Connection, mint: PublicKey) => Promise<Metadata>;

type GetWalletTokens = (publicKey: PublicKey, connection: Connection) => Promise<Token[]>;

type GetNFT = (
  connection: Connection,
  mint: PublicKey,
  assTokenAccount?: PublicKey,
  stakedAccount?: StakedTokenAccount,
  stakedAccountPublicKey?: PublicKey
) => Promise<NFT | void>;

type Attribute = {
  trait_type: string;
  value: string;
};

type NFT = {
  mint: PublicKey;
  owner: PublicKey;
  metadata: FetchedMetaData;
  stakedAccountPublicKey?: PublicKey;
  stakedAccount?: StakedTokenAccount;
  assTokenAccount?: PublicKey;
  isV2?: boolean;
  isLegendary: boolean;
};

type Token = {
  publicKey: PublicKey;
  tokenAccount: PublicKey;
  balance: number;
};

export type {
  GetMetaDataByMint,
  GetNFT,
  GetWalletTokens,
  NFT,
  Token,
  StakedTokenAccount,
  //
  AccountInfo,
  BN,
  Metadata,
  MetadataData,
};
