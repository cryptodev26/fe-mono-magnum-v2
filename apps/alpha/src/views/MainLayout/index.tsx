import { WalletButton } from 'components';
import { useHydration } from 'contexts/Hydrate';
import { useMediaQuery } from 'hooks/useMediaQuery';
import React from 'react';
import { Toaster } from 'react-hot-toast';
import { Footer, Nav } from 'shared-ui';
import { styles } from 'styles';

export default function MainLayout({ children }: { children: JSX.Element }) {
  const hydrated = useHydration();
  const match = useMediaQuery('(min-width: 490px)');

  return (
    <>
      {hydrated && <Nav button={match ? <WalletButton /> : <div />} />}
      <div css={styles.container}>{children}</div>
      <Footer />
      <Toaster
        position='bottom-left'
        gutter={10}
        toastOptions={{
          // Define default options
          className: '',
          duration: 5000,
          style: { background: '#191919', color: '#fff' },

          success: {
            duration: 3000,
            iconTheme: { primary: '#a012dd', secondary: 'black' },
          },

          error: {
            duration: 3000,
            iconTheme: { primary: '#c41f29', secondary: 'black' },
          },
        }}
      />
    </>
  );
}
