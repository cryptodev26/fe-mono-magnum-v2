import React from 'react';
import Link from 'next/link';
import { AnimatePresence } from 'framer-motion';
import { Containers, Modal, Spinner } from 'shared-ui';
import { WalletButton } from 'components/WalletButton';
import 'twin.macro';

import {
  ActionBanner,
  ConditionalWrapper,
  NftFeature,
  NftTile,
  SkeletonGrid,
  StakeButton,
} from 'components';
import { styles } from 'styles';

import { useMediaQuery } from 'hooks/useMediaQuery';
import { useStakingPage } from './useStakingPage';
import { Toolbar } from './Toolbar';
import StakingStats from './StakingStats';
import Connect from './Connect';

const Staking = () => {
  const {
    router,
    error,
    loading,
    connecting,
    connected,
    nfts,
    selection,
    view,
    selectView,
    handleFilterClick,
    handleSelectView,
    handleSelect,
    closeModal,
    isSelected,
    stats,
  } = useStakingPage();

  const match = useMediaQuery('(max-width: 489px)');

  if (error)
    return (
      <>
        <h1>Uh oh</h1>
        <p>Something went wrong. Please try again in a bit.</p>
      </>
    );

  if (loading || connecting)
    return (
      <>
        <Spinner />
        <SkeletonGrid />
      </>
    );

  return !connected ? (
    <Connect stats={stats} />
  ) : (
    <>
      <main css={styles.main}>
        {match && (
          <div tw='flex justify-center mb-8'>
            <WalletButton />
          </div>
        )}

        <div tw='mt-5'>
          <StakingStats
            horizontal
            stakedNFTs={nfts.staked}
          />
        </div>

        <div tw='flex flex-col'>
          <Toolbar
            nfts={nfts}
            view={view}
            selectView={selectView}
            handleFilterClick={handleFilterClick}
            handleSelectView={handleSelectView}
          />
          <Containers.Grid.Base css={styles.centerFit}>
            {nfts[view].map(nft => (
              <ConditionalWrapper
                condition={!selectView}
                renderWrap={children => (
                  <Link
                    as={`/${nft.mint.toString()}`}
                    href={`?nft=${nft.mint.toString()}`}
                  >
                    {children}
                  </Link>
                )}
                key={`${
                  nft.stakedAccount && nft.stakedAccountPublicKey
                }-${nft.mint.toString()}`}
              >
                <NftTile
                  nft={nft}
                  featured
                  glow={isSelected(nft) && selectView}
                  badge
                  onClick={() => handleSelect(nft)}
                />
              </ConditionalWrapper>
            ))}
          </Containers.Grid.Base>
        </div>
      </main>
      <AnimatePresence>
        {router.query.nft && (
          <Modal
            open={!!router.query.nft}
            handleClose={closeModal}
          >
            <NftFeature />
          </Modal>
        )}
        {selectView &&
          (view === 'staked'
            ? selection.staked.length > 0
            : selection.wallet.length > 0) && (
            <ActionBanner>
              {view === 'staked' ? (
                <StakeButton
                  variant='unstake'
                  nfts={selection.staked}
                  tw='max-w-[350px] w-full'
                />
              ) : (
                <StakeButton
                  variant='stake'
                  nfts={selection.wallet}
                  tw='max-w-[350px] w-full'
                />
              )}
            </ActionBanner>
          )}
      </AnimatePresence>
    </>
  );
};

export default Staking;
