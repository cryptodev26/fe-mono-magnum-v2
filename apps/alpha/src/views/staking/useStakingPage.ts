import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import sameOrAfter from 'dayjs/plugin/isSameOrAfter';
import { useCallback, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import { NFT } from 'types';
import { usePrograms } from 'contexts';
import hashes from 'data/hashes.json';
import useUserStore from 'stores/useUserStore';
import toast from 'react-hot-toast';

dayjs.extend(relativeTime);
dayjs.extend(sameOrAfter);

export const useStakingPage = () => {
  const router = useRouter();
  const { nfts } = useUserStore();
  const [view, setView] = useState<'staked' | 'wallet'>('wallet');
  const [selectView, setSelectView] = useState<boolean>(false);
  const [selection, setSelection] = useState<{ wallet: NFT[]; staked: NFT[] }>({
    wallet: [],
    staked: [],
  });
  const [stats, setStats] = useState<{
    totalValueStaked: number;
    totalStaked: number;
    percentage: string;
  }>({ totalValueStaked: 0, totalStaked: 0, percentage: '0%' });

  const {
    loading,
    error,
    stakingProgram,
    wallet: { connecting, connected },
    event,
  } = usePrograms();

  useEffect(() => {
    const all = async () => {
      const allStakedTokens = await stakingProgram?.account.stakeAccount.all();

      if (!allStakedTokens) return;

      const totalStaked = allStakedTokens.length;
      const percentage = `${((totalStaked / hashes.length) * 100).toFixed(2)}%`;

      const getTotalLockupRewards = (
        lockup: 0 | 1 | 2,
        legenadry: boolean,
        daily: number
      ) =>
        allStakedTokens.filter(
          token =>
            token.account.stakingPeriod === lockup &&
            token.account.isOneOfOne === legenadry
        ).length * daily;

      const totalValueStaked =
        getTotalLockupRewards(0, false, 5) +
        getTotalLockupRewards(0, true, 7) +
        getTotalLockupRewards(1, false, 7) +
        getTotalLockupRewards(1, true, 10) +
        getTotalLockupRewards(2, false, 10) +
        getTotalLockupRewards(2, true, 15);

      setStats({ totalValueStaked, totalStaked, percentage });
    };

    all();
  }, [stakingProgram]);

  // check if user has nfts in the selected view
  useEffect(() => {
    if (nfts[view].length < 1) setView(prev => (prev === 'wallet' ? 'staked' : 'wallet'));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nfts]);

  const handleFilterClick = useCallback((filter: 'staked' | 'wallet') => {
    setView(filter);
  }, []);

  const handleSelectView = useCallback(() => {
    setSelectView(!selectView);
  }, [selectView]);

  const handleSelect = useCallback(
    (nft: NFT) => {
      if (!selectView) return;

      if (nft.stakedAccount) {
        if (!nft.stakedAccount?.isV2)
          return toast.error(
            'Tokens staked in the legacy program must be unstaked one by one'
          );
        if (
          dayjs().isBefore(dayjs.unix(Number(nft.stakedAccount.unstakeDate.toString())))
        )
          return toast.error('Token cannot be unstaked yet');

        if (
          !selection.staked.find(token => token.mint.toString() === nft.mint.toString())
        ) {
          if (selection.staked.length === 4)
            return toast.error('Exceeds maximum payload');
          return setSelection(prev => ({ ...prev, staked: [...prev.staked, nft] }));
        }

        return setSelection(prev => ({
          ...prev,
          staked: prev.staked.filter(
            token => token.mint.toString() !== nft.mint.toString()
          ),
        }));
      }

      if (!nft.stakedAccount)
        if (
          !selection.wallet.find(token => token.mint.toString() === nft.mint.toString())
        ) {
          if (selection.wallet.length === 4)
            return toast.error('Exceeds maximum payload');
          return setSelection(prev => ({ ...prev, wallet: [...prev.wallet, nft] }));
        }

      return setSelection(prev => ({
        ...prev,
        wallet: prev.wallet.filter(
          token => token.mint.toString() !== nft.mint.toString()
        ),
      }));
    },
    [selectView, selection]
  );

  const closeModal = useCallback(() => {
    document.body.style.position = '';
    document.body.style.top = '';
    return router.back();
  }, [router]);

  const isSelected = (nft: NFT) =>
    !![...selection.staked, ...selection.wallet].find(
      token => token.mint.toString() === nft.mint.toString()
    );

  // const clearSelection = useCallback((type: 'staked' | 'wallet') => {
  //   console.log('you called?');
  //   setSelection(prev => ({ ...prev, [type]: [] }));
  // }, []);

  useEffect(() => {
    if (event?.status === 'success') {
      setTimeout(() => {
        setSelection({ staked: [], wallet: [] });
      }, 2000);
    }
  }, [event]);

  useEffect(() => console.log(selection), [selection]);

  return {
    router,
    error,
    loading,
    // clearSelection,
    connecting,
    connected,
    nfts,
    view,
    selection,
    selectView,
    stats,
    handleFilterClick,
    handleSelectView,
    handleSelect,
    closeModal,
    isSelected,
  };
};
