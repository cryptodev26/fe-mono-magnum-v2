import 'twin.macro';
import React, { FC } from 'react';

import { Card, Divider } from 'shared-ui';
import { estimatedTotalRewards, estimatedDailyRewards } from 'utils/helpers/web3';
import { NFT, SummaryAccount } from 'types';

interface StakingStatsProps {
  stakedNFTs?: NFT[];
  summaryStaking?: SummaryAccount;
  horizontal?: boolean;
}

// TODO: How can I add a conditional style adding it inside tw?

const StakingStats: FC<StakingStatsProps> = ({
  stakedNFTs,
  horizontal,
  summaryStaking,
}) => {
  if (horizontal) {
    return (
      <div tw='mb-5'>
        <Card.Base featured>
          <Card.Body tw='grid sm:grid-cols-2 lg:grid-cols-3 lg:items-center gap-x-5'>
            <div tw='p-2 text-center relative'>
              <div>
                <h3 tw='mb-2 lg:mb-5 lg:text-2xl'>
                  {summaryStaking ? 'Current Staked' : 'Total Staked'}
                </h3>
                <p tw='text-lg lg:text-xl'>
                  {summaryStaking ? summaryStaking.currentStaked : stakedNFTs?.length}
                </p>
              </div>

              <Divider
                col
                tw='hidden sm:block absolute right-[-20px] top-[-10px]'
              />
            </div>

            <Divider tw='w-4/5 justify-self-center sm:hidden' />

            <div tw='p-2 text-center relative'>
              <div>
                <h3 tw='mb-2 lg:mb-5 lg:text-2xl'>
                  {summaryStaking ? 'Total Collected' : 'Total Reward'}
                </h3>
                <div tw='flex justify-center items-center gap-x-1 lg:gap-x-2 text-lg lg:text-xl'>
                  {summaryStaking || stakedNFTs?.length !== 0 ? (
                    <>
                      <p>
                        {summaryStaking
                          ? summaryStaking.totalCollected
                          : estimatedTotalRewards(stakedNFTs).toFixed(2)}{' '}
                        $MAGAI
                      </p>
                      <picture>
                        <source
                          src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
                          type='image/gif'
                          tw='w-6 sm:w-8 lg:w-9'
                        />
                        <img
                          src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
                          alt='coin'
                          tw='w-6 sm:w-8 lg:w-9'
                        />
                      </picture>
                    </>
                  ) : (
                    <p>0</p>
                  )}
                </div>
              </div>

              <Divider
                col
                tw='hidden lg:block absolute right-[-20px] top-[-10px]'
              />
            </div>

            <Divider tw='w-4/5 justify-self-center sm:hidden' />

            <div tw='p-2 text-center'>
              <h3 tw='mb-2 lg:mb-5 lg:text-2xl'>
                {summaryStaking ? 'Stake History' : 'Rewards Per Day'}
              </h3>
              <div tw='flex justify-center items-center gap-x-1 lg:gap-x-2 text-lg lg:text-xl'>
                {summaryStaking || stakedNFTs?.length !== 0 ? (
                  <>
                    <p>
                      {summaryStaking
                        ? summaryStaking.stakeHistory
                        : estimatedDailyRewards(stakedNFTs).toFixed(2)}{' '}
                      $MAGAI
                    </p>
                    <picture>
                      <source
                        src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
                        type='image/gif'
                        tw='w-6 sm:w-8 lg:w-9'
                      />
                      <img
                        src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
                        alt='coin'
                        tw='w-6 sm:w-8 lg:w-9'
                      />
                    </picture>
                    <p>/ day</p>
                  </>
                ) : (
                  <p>0</p>
                )}
              </div>
            </div>
          </Card.Body>
        </Card.Base>
      </div>
    );
  }

  return (
    <Card.Base featured>
      <Card.Body tw='grid grid-cols-1 gap-x-5'>
        <div tw='p-2 text-center relative'>
          <div>
            <h3 tw='mb-2 lg:mb-5 lg:text-2xl'>
              {summaryStaking ? 'Current Staked' : 'Total Staked'}
            </h3>
            <p tw='text-lg lg:text-xl'>
              {summaryStaking ? summaryStaking.currentStaked : stakedNFTs?.length}
            </p>
          </div>
        </div>

        <Divider tw='w-4/5 justify-self-center' />

        <div tw='p-2 text-center relative'>
          <div>
            <h3 tw='mb-2 lg:mb-5 lg:text-2xl'>
              {summaryStaking ? 'Total Collected' : 'Total Reward'}
            </h3>
            <div tw='flex justify-center items-center gap-x-1 lg:gap-x-2 text-lg lg:text-xl'>
              {summaryStaking || stakedNFTs?.length !== 0 ? (
                <>
                  <p>
                    {summaryStaking
                      ? summaryStaking.totalCollected
                      : estimatedTotalRewards(stakedNFTs).toFixed(2)}{' '}
                    $MAGAI
                  </p>
                  <picture>
                    <source
                      src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
                      type='image/gif'
                      tw='w-6 sm:w-8 lg:w-9'
                    />
                    <img
                      src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
                      alt='coin'
                      tw='w-6 sm:w-8 lg:w-9'
                    />
                  </picture>
                </>
              ) : (
                <p>0</p>
              )}
            </div>
          </div>
        </div>

        <Divider tw='w-4/5 justify-self-center' />

        <div tw='p-2 text-center'>
          <h3 tw='mb-2 lg:mb-5 lg:text-2xl'>
            {summaryStaking ? 'Stake History' : 'Rewards Per Day'}
          </h3>
          <div tw='flex justify-center items-center gap-x-1 lg:gap-x-2 text-lg lg:text-xl'>
            {summaryStaking || stakedNFTs?.length !== 0 ? (
              <>
                <p>
                  {summaryStaking
                    ? summaryStaking.stakeHistory
                    : estimatedDailyRewards(stakedNFTs).toFixed(2)}
                </p>
                {!summaryStaking && (
                  <>
                    <p>$MAGAI</p>
                    <picture>
                      <source
                        src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
                        type='image/gif'
                        tw='w-6 sm:w-8 lg:w-9'
                      />
                      <img
                        src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
                        alt='coin'
                        tw='w-6 sm:w-8 lg:w-9'
                      />
                    </picture>
                    <p>/ day</p>
                  </>
                )}
              </>
            ) : (
              <p>0</p>
            )}
          </div>
        </div>
      </Card.Body>
    </Card.Base>
  );
};

export default StakingStats;
