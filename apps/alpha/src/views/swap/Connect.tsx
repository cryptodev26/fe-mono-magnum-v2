import { WalletButton } from 'components';

import React from 'react';
import { Containers, Divider } from 'shared-ui';
import { styles } from 'styles';
import 'twin.macro';

function Stat({ title, value }: { title: string; value: string | number }) {
  return (
    <div tw='py-5 px-16 lg:px-20 text-center'>
      <h4>{title}</h4>
      <h6 tw='mt-5'>{value}</h6>
      <Divider />
    </div>
  );
}

export default function Connect({
  stats,
}: {
  stats: {
    totalValueStaked: number;
    totalStaked: number;
    percentage: string;
  };
}) {
  return (
    <main
      css={styles.main}
      tw='relative min-h-[auto] h-[fit-content] pt-20'
    >
      <Containers.Grid.Base>
        <Containers.Grid.Cell
          col={12}
          md={7}
        >
          <h1>Stasis</h1>
          <div tw='flex gap-5 items-center'>
            <p>Stake your Magnums and earn $MAGAI</p>
          </div>
          <Divider />
          <WalletButton variant='text' />
          <Containers.Img tw='w-[55%] mt-10'>
            <picture>
              <source
                src='/staking.gif'
                type='image/gif'
              />
              <img
                src='/staking.gif'
                alt='Staking Animation'
              />
            </picture>
          </Containers.Img>
        </Containers.Grid.Cell>
        <Containers.Grid.Cell
          col={12}
          md={5}
          tw='flex flex-col justify-between'
        >
          <Stat
            title='Staked Magnums'
            value={stats.totalStaked}
          />
          <Stat
            title='Total Value Locked'
            value={stats.totalValueStaked}
          />
          <Stat
            title='% of Magnums Staked'
            value={stats.percentage}
          />
        </Containers.Grid.Cell>
      </Containers.Grid.Base>
    </main>
  );
}
