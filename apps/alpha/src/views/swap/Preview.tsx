import React, { Component } from 'react';
import * as PIXI from 'pixi.js';

type PreviewProps = {
  keys: any;
  formData: any;
};

export default class Preview extends Component<PreviewProps> {
  app: any;

  animatedSprites: any;

  staticSprites: any;

  componentDidMount(): void {
    const { formData, keys } = this.props;
    this.app = new PIXI.Application({
      backgroundColor: 0x000000,
      resizeTo: document.getElementById('sceneContainer') as HTMLElement,
    });

    document.getElementById('sceneContainer')?.replaceChildren(this.app.view);

    this.animatedSprites = [];
    this.staticSprites = [];

    keys.map((attr: any, index: number) => {
      console.log(formData[attr]);
      const baseUrl = `https://magnum-ai-animated-public.s3.us-west-1.amazonaws.com/All_Animated_Assets/${attr}/${formData[attr].name}/${formData[attr].name}`;
      this.setAnimatedSprite(index, baseUrl, true);
      this.setStaticSprite(index, baseUrl);
      this.addStaticSprite(index);
      return true;
    });
  }

  componentDidUpdate(prevProps: PreviewProps) {
    const { formData, keys } = this.props;
    if (prevProps.formData !== formData) {
      keys.map((attr: any, index: number) => {
        const baseUrl = `https://magnum-ai-animated-public.s3.us-west-1.amazonaws.com/All_Animated_Assets/${attr}/${formData[attr].name}/${formData[attr].name}`;

        if (formData[attr].animation) {
          this.removeStaticSprite(index);
          this.removeAnimatedSprite(index);
          this.setAnimatedSprite(index, baseUrl, false);
          this.addAnimatedSprite(index);
        } else {
          this.removeAnimatedSprite(index);
          this.setStaticSprite(index, baseUrl);
          this.addStaticSprite(index);
        }
        return true;
      });
    }
  }

  componentWillUnmount() {
    this.app.destroy(true, { children: true, texture: true, baseTexture: true });
  }

  setStaticSprite(id: number, baseUrl: string) {
    if (baseUrl === '') return;

    this.staticSprites[id] = PIXI.Sprite.from(`${baseUrl}_1.png`);
    this.staticSprites[id].x = 0;
    this.staticSprites[id].y = 0;
    this.staticSprites[id].width = this.app.view.width;
    this.staticSprites[id].height = this.app.view.height;
    this.staticSprites[id].anchor.set(0, 0);
  }

  setAnimatedSprite(id: number, baseUrl: string, isFirst: boolean) {
    if (baseUrl === '') return;

    const frames = [];
    for (let i = 1; i <= 40; i += 1) {
      const texture = PIXI.Texture.from(`${baseUrl}_${i}.png`);
      frames.push(texture);
    }
    this.animatedSprites[id] = new PIXI.AnimatedSprite(frames);
    this.animatedSprites[id].x = 0;
    this.animatedSprites[id].y = 0;
    this.animatedSprites[id].width = isFirst
      ? this.app.view.width / 720
      : this.app.view.width;
    this.animatedSprites[id].height = isFirst
      ? this.app.view.height / 720
      : this.app.view.height;
    this.animatedSprites[id].loop = true;
    this.animatedSprites[id].animationSpeed = 0.5;
    this.animatedSprites[id].interactive = true;
    this.animatedSprites[id].anchor.set(0, 0);
  }

  removeAnimatedSprite(id: number) {
    this.app.stage.removeChild(this.animatedSprites[id]);
    this.animatedSprites[id].stop();
  }

  removeStaticSprite(id: number) {
    this.app.stage.removeChild(this.staticSprites[id]);
  }

  addAnimatedSprite(id: number) {
    this.animatedSprites[id].play();
    this.app.stage.addChild(this.animatedSprites[id]);
  }

  addStaticSprite(id: number) {
    this.app.stage.addChild(this.staticSprites[id]);
  }

  render() {
    return (
      <div
        id='sceneContainer'
        style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
          top: 0,
        }}
      />
    );
  }
}
