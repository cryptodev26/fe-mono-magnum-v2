import React from 'react';
import { Badge, Form } from 'shared-ui';
import tw, { styled } from 'twin.macro';

import { UserNFTs } from 'types';

const BadgeButton = styled(Badge)`
  ${tw`cursor-pointer hover:bg-neutral-800`}
`;

interface ToolbarProps {
  nfts: UserNFTs;
  view: 'wallet' | 'staked';
  selectView: boolean;
  handleFilterClick(filter: 'wallet' | 'staked'): void;
  handleSelectView(): void;
}

export const Toolbar = ({
  nfts,
  view,
  selectView,
  handleFilterClick,
  handleSelectView,
}: ToolbarProps) => {
  return (
    <Form.FieldSet tw='flex justify-center gap-2 mb-10 max-w-[fit-content] ml-auto'>
      {['wallet', 'staked', 'select'].map(filter =>
        filter !== 'select' ? (
          nfts[filter as 'wallet' | 'staked'].length > 0 && (
            <BadgeButton
              onClick={() => handleFilterClick(filter as 'wallet' | 'staked')}
              variant={view === filter ? 'primary' : 'neutral'}
              key={filter}
            >
              {filter === 'wallet' ? 'Not staked' : 'Staked'}
            </BadgeButton>
          )
        ) : (
          <BadgeButton
            onClick={handleSelectView}
            variant={selectView ? 'primary' : 'neutral'}
            key={filter}
          >
            Select
          </BadgeButton>
        )
      )}
    </Form.FieldSet>
  );
};
