import React, { useState, useRef, useEffect } from 'react';
import { Spinner } from 'shared-ui';
import { WalletButton } from 'components/WalletButton';
import 'twin.macro';
import SectionView from 'components/SectionView';
import ImageContainer from 'components/ImageContainer';
import Form from 'components/Form';
import { toTitle } from 'utils/helpers/helpers';

// Get NFT in wallet
import { PublicKey } from 'types';
import { usePrograms } from 'contexts';
import { getWalletNfts } from 'utils/helpers/web3';

import { SkeletonGrid } from 'components';
import { styles } from 'styles';

import { useMediaQuery } from 'hooks/useMediaQuery';
import { useSwapPage } from './useSwapPage';
import Connect from './Connect';
import Scene from './Scene';
import Preview from './Preview';
import attributes from '../../data/images.json';

const Attribute = (props: any) => {
  const { zIndex: z, src } = props;
  return (
    <div
      tw='absolute top-0 left-0 bg-contain	bg-no-repeat	bg-center w-full h-full transition duration-200 ease-in'
      style={{
        zIndex: z,
        backgroundImage: `url(${encodeURI(src)})`,
      }}
    />
  );
};

const Swap = () => {
  const [display, setDisplay] = useState('gif');
  const { error, loading, connecting, connected, stats } = useSwapPage();
  const match = useMediaQuery('(max-width: 489px)');
  const {
    wallet: { publicKey },
    connection,
  } = usePrograms();

  const [attrState] = useState<any>(attributes);

  const blankObj = { _id: '', name: '', data: '', rating: 0, __v: '', animation: false };
  const [formData, setFormData] = useState<any>({
    backgrounds: blankObj,
    bodies: blankObj,
    costumes: blankObj,
    faces: blankObj,
    glasses: blankObj,
    face_gear: blankObj,
    head_gear: blankObj,
  });

  const attrKeys = Object.entries(attrState).map(arr => arr[0]);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const properties = Object.entries(attrState).map(() => useRef(null));

  const handleDisplay = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setDisplay(e.target.value);
  };

  const handleSelect = (e: React.ChangeEvent<HTMLSelectElement>) => {
    if (e.target.value === '') return;

    setFormData({
      ...formData,
      [e.target.name]: attrState[e.target.name].find(
        (obj: any) => obj.name === e.target.value
      ),
    });
  };

  const animation = (title: string) => {
    setFormData({
      ...formData,
      [title]: {
        ...formData[title],
        animation:
          formData[title].animation === undefined ? true : !formData[title].animation,
      },
    });
  };

  const getConvertedIndex = (index: string) => {
    // eslint-disable-next-line default-case
    switch (index) {
      case 'Background':
        return 'backgrounds';
      case 'Body':
        return 'bodies';
      case 'Face':
        return 'faces';
      case 'Costume':
        return 'costumes';
      case 'Face Gear':
        return 'face_gear';
      case 'Glasses':
        return 'glasses';
      case 'Headgear':
        return 'head_gear';
      default:
        return 'backgrounds';
    }
  };

  useEffect(() => {
    async function getAttributes() {
      const magnumNFTs = await getWalletNfts(publicKey as PublicKey, connection);
      if (magnumNFTs?.length) {
        const rawData = magnumNFTs?.[0]?.metadata?.attributes;
        const trait: any = {};
        // eslint-disable-next-line no-restricted-syntax
        for (const property of rawData) {
          if (property?.trait_type) {
            // eslint-disable-next-line @typescript-eslint/no-use-before-define
            trait[getConvertedIndex(property?.trait_type)] = {
              _id: property?.value?.replace(/ /g, '_')?.toLowerCase(),
              name: property?.value?.replace(/ /g, '_'),
              data: '',
              rating: 0,
              animation: false,
              __v: '',
            };
          }
        }
        setFormData({
          ...formData,
          ...trait,
        });
      }
    }
    if (connection && publicKey) {
      getAttributes();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connection, publicKey]);

  if (error)
    return (
      <>
        <h1>Uh oh</h1>
        <p>Something went wrong. Please try again in a bit.</p>
      </>
    );

  if (loading || connecting)
    return (
      <>
        <Spinner />
        <SkeletonGrid />
      </>
    );

  return !connected ? (
    <Connect stats={stats} />
  ) : (
    <main
      css={styles.main}
      style={{ maxWidth: '100% !important' }}
    >
      {match && (
        <div tw='flex justify-center mb-8'>
          <WalletButton />
        </div>
      )}
      <Form.Group>
        <div tw='w-full flex justify-between items-center'>
          <Form.Label
            group
            htmlFor='Display'
            style={{ textTransform: 'uppercase' }}
          >
            Display
          </Form.Label>
        </div>
        <div tw='flex'>
          <Form.Select
            style={{
              width: 'auto',
            }}
            onChange={handleDisplay}
            name='Display'
            value={display}
          >
            <option value='gif'>With GIF</option>
            <option value='pixi'>With Pixi</option>
          </Form.Select>
        </div>
      </Form.Group>
      <SectionView
        position='left'
        title=''
        tw='w-full pt-6'
      >
        <div tw='flex md:flex-row flex-col justify-between w-full lg:gap-12 gap-4'>
          <ImageContainer
            square
            tw='border-[4px] border-white rounded-lg'
            style={{ flex: 3 }}
          >
            {display === 'gif' ? (
              attrKeys.map(attr => (
                <Attribute
                  key={attr}
                  src={
                    formData[attr].animation
                      ? `/images/rarities/${attr}/${formData[attr].name.replace(
                          '_',
                          ' '
                        )}.gif`
                      : `/images/rarities/${attr}/${formData[attr].name.replace(
                          '_',
                          ' '
                        )}.png`
                  }
                  zIndex={attr === 'backgrounds' ? 1 : 3}
                />
              ))
            ) : (
              <Preview
                keys={attrKeys}
                formData={formData}
              />
            )}
          </ImageContainer>
          <div style={{ flex: 5 }}>
            <div tw='border-[4px] border-white rounded-lg pb-4 lg:pb-10'>
              <div tw='grid lg:grid-cols-4 grid-cols-1 sm:grid-cols-2 col-gap-6 row-gap-12 p-4 lg:p-10'>
                {attrKeys.map((title: string, index: number) => (
                  <div
                    key={title}
                    style={{ position: 'relative' }}
                  >
                    <Form.Group>
                      <div tw='w-full flex justify-between items-center'>
                        <Form.Label
                          group
                          htmlFor={title}
                          style={{ textTransform: 'uppercase' }}
                        >
                          {title}
                        </Form.Label>
                      </div>
                      <Form.Select
                        name={title}
                        value={formData[title].name}
                        onChange={handleSelect}
                        disabled={title === 'faces' && formData.faces.name === 'face'}
                        style={{
                          fontSize: 12,
                        }}
                      >
                        <option
                          value=''
                          style={{ fontSize: 14 }}
                        >
                          Select One
                        </option>
                        {attrState[title].map((attr: any) => (
                          <option
                            key={attr?.name?.concat(attr?.title)}
                            value={attr.name}
                            style={{ fontSize: 14 }}
                          >
                            {toTitle(attr.name)}
                          </option>
                        ))}
                      </Form.Select>
                    </Form.Group>
                    <ImageContainer
                      square
                      tw='border-[1px] border-white rounded-lg'
                      ref={properties[index]}
                      onClick={() => animation(title)}
                      style={{
                        cursor: 'pointer',
                      }}
                    >
                      {display === 'gif' ? (
                        <Attribute
                          key={title}
                          src={
                            formData[title].animation
                              ? `/images/rarities/${title}/${formData[title].name.replace(
                                  '_',
                                  ' '
                                )}.gif`
                              : `/images/rarities/${title}/${formData[title].name.replace(
                                  '_',
                                  ' '
                                )}.png`
                          }
                          zIndex={title === 'backgrounds' ? 1 : 3}
                        />
                      ) : (
                        <Scene
                          src={`https://magnum-ai-animated-public.s3.us-west-1.amazonaws.com/All_Animated_Assets/${title}/${formData[title].name}/${formData[title].name}`}
                          animation={formData[title].animation}
                          index={index}
                        />
                      )}
                    </ImageContainer>
                  </div>
                ))}
              </div>
              <div tw='flex justify-center'>
                <Form.Button
                  success
                  tw='border-2	border-cyan-400'
                >
                  Display
                </Form.Button>
              </div>
            </div>
            <div tw='flex flex-col lg:flex-row xl:flex-row items-center gap-12 md:gap-6 pt-6'>
              <Form.Button
                success
                tw='border-2 border-cyan-400'
                style={{ flex: 2 }}
              >
                Button 1
              </Form.Button>
              <Form.Button
                success
                tw='border-2 border-cyan-400'
                style={{ flex: 1 }}
              >
                Button 2
              </Form.Button>
            </div>
          </div>
        </div>
      </SectionView>
    </main>
  );
};

export default Swap;
