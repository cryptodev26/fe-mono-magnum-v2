import React, { Component } from 'react';
import * as PIXI from 'pixi.js';

type SceneProps = {
  animation: boolean;

  index: number;

  src: string;
};

export default class Scene extends Component<SceneProps> {
  app: any;

  animatedSprite: any;

  staticSprite: any;

  componentDidMount(): void {
    const { index, src } = this.props;
    this.app = new PIXI.Application({
      backgroundColor: 'transparent',
      resizeTo: document.getElementById(`sceneContainer${index}`) as HTMLElement,
    });

    document.getElementById(`sceneContainer${index}`)?.replaceChildren(this.app.view);

    // for static
    this.setStatisSprite(src);
    this.addStaticSprite();

    // for animation
    this.setAnimatedSprite(src, true);
  }

  componentDidUpdate(prevProps: SceneProps) {
    const { src, animation } = this.props;
    if (prevProps.src !== src) {
      this.setStaticTexture(src);
    }

    if (prevProps.animation !== animation) {
      if (animation) {
        this.removeStaticSprite();
        this.removeAnimatedSprite();
        this.setAnimatedSprite(src, false);
        this.addAnimatedSprite();
      } else {
        this.removeAnimatedSprite();
        this.removeStaticSprite();
        this.setStatisSprite(src);
        this.addStaticSprite();
      }
    }
  }

  componentWillUnmount() {
    this.app.destroy(true, { children: true, texture: true, baseTexture: true });
  }

  setAnimatedSprite(baseUrl: string, isFirst: boolean) {
    if (baseUrl === '') return;

    const frames = [];
    for (let i = 1; i <= 40; i += 1) {
      const texture = PIXI.Texture.from(`${baseUrl}_${i}.png`);
      frames.push(texture);
    }
    this.animatedSprite = new PIXI.AnimatedSprite(frames);
    this.animatedSprite.x = 0;
    this.animatedSprite.y = 0;
    this.animatedSprite.width = isFirst ? this.app.view.width / 720 : this.app.view.width;
    this.animatedSprite.height = isFirst
      ? this.app.view.height / 720
      : this.app.view.height;
    this.animatedSprite.loop = true;
    this.animatedSprite.animationSpeed = 0.5;
    this.animatedSprite.anchor.set(0, 0);
  }

  setStatisSprite(baseUrl: string) {
    if (baseUrl === '') return;

    this.staticSprite = PIXI.Sprite.from(`${baseUrl}_1.png`);
    this.staticSprite.x = 0;
    this.staticSprite.y = 0;
    this.staticSprite.width = this.app.view.width;
    this.staticSprite.height = this.app.view.height;
    this.staticSprite.anchor.set(0, 0);
  }

  setStaticTexture(baseUrl: string) {
    this.staticSprite.texture = PIXI.Texture.from(`${baseUrl}_1.png`);
    this.staticSprite.texture.update();
  }

  removeAnimatedSprite() {
    this.app.stage.removeChild(this.animatedSprite);
    this.animatedSprite.stop();
  }

  removeStaticSprite() {
    this.app.stage.removeChild(this.staticSprite);
  }

  addAnimatedSprite() {
    this.animatedSprite.play();
    this.app.stage.addChild(this.animatedSprite);
  }

  addStaticSprite() {
    this.app.stage.addChild(this.staticSprite);
  }

  render() {
    const { index } = this.props;
    return (
      <div
        id={`sceneContainer${index}`}
        style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
          top: 0,
        }}
      />
    );
  }
}
