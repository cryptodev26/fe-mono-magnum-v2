import tw from 'twin.macro';

export const styles = {
  container: tw`flex flex-col items-center justify-center h-full min-h-[calc(100vh - 5rem)] lg:(max-w-[1900px] mx-auto) pt-20 pb-20`,

  main: tw`p-5 w-full h-full min-h-[calc(100vh - 5rem)]`,

  centerFit: tw`w-[fit-content] mx-auto`,

  buttonSpinner: tw`w-4 h-4  animate-spin`,

  buttonDivider: tw`mx-1.5 border-r border-l border-neutral-600`,

  toastContainer: tw`bg-black`,
};
