import React from 'react';
import { AppProps } from 'next/app';
import { Styles } from 'shared-ui';
import Head from 'next/head';
import { Web3Provider, StaticApiProvider } from 'contexts';

import HydrationProvider from 'contexts/Hydrate';
import MainLayout from 'views/MainLayout';
import { DataProvider } from '../contexts/store/store.context';

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <Head>
        <title>Magnum AI</title>
      </Head>

      <HydrationProvider>
        <StaticApiProvider>
          <Web3Provider>
            <DataProvider>
              <Styles.Global />
              <MainLayout>
                <Component {...pageProps} />
              </MainLayout>
            </DataProvider>
          </Web3Provider>
        </StaticApiProvider>
      </HydrationProvider>
    </>
  );
};

export default App;
