import { usePrograms } from 'contexts';
import { useEffect, useState } from 'react';

import { SummaryAccount } from 'types';
import { getStakingSummary } from 'utils/helpers';

const useStakingSummary = () => {
  const { stakingProgram } = usePrograms();
  const [stakingSummary, setStakingSummary] = useState<SummaryAccount>();

  useEffect(() => {
    const getSummary = async () => {
      if (!stakingProgram) return;

      const { summaryAccount } = await getStakingSummary(stakingProgram);

      setStakingSummary(summaryAccount);
    };

    getSummary();
  }, [stakingProgram]);

  return { stakingSummary };
};

export default useStakingSummary;
