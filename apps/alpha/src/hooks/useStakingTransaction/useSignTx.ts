import { Keypair, SignTransactionHook, Transaction } from 'types';

const useSignTx: SignTransactionHook = (wallet, connection) => {
  const signTx = async (transaction: Transaction, signers?: Keypair[]) => {
    try {
      if (transaction.instructions.length === 0) return new Error('transaction failed');

      const signature = await wallet.sendTransaction(transaction, connection, {
        signers,
      });

      const latestBlockhash = await connection.getLatestBlockhash();

      const result = await connection.confirmTransaction({
        signature,
        ...latestBlockhash,
      });

      return result;
    } catch (err) {
      console.error(err);
    }
  };

  return signTx;
};

export default useSignTx;
