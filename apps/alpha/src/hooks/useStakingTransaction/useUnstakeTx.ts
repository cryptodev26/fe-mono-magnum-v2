import { useState } from 'react';
import { TransactionInstruction } from '@solana/web3.js';

import { NFT, TransactionHook } from 'types';
import {
  createCollectionInstructions,
  createUnstakingInstructions,
  initTransaction,
  unstakeLegacy,
} from 'utils/helpers/web3';

const useUnstakeTx: TransactionHook = (stakingProvider, stakingProgram, wallet) => {
  const [error, setError] = useState<Error>();

  const unstakeTx = async (nfts: NFT[]) => {
    try {
      let transaction = await initTransaction(stakingProgram, wallet);
      if (!transaction) throw new Error('transaction could not be initiated');

      await Promise.all(
        nfts.map(async nft => {
          const instructions: TransactionInstruction[] = [];
          try {
            if (!nft.stakedAccount || !nft.stakedAccountPublicKey)
              throw new Error('no staking account found');

            if (!nft.stakedAccount.isV2 && stakingProgram) {
              const res = await unstakeLegacy(stakingProvider, stakingProgram, nft);
              return res;
            }

            const collection = await createCollectionInstructions(
              stakingProvider,
              stakingProgram,
              nft
            );

            if (collection.fallbackTx) transaction = collection.fallbackTx;

            const collectionInstructions = await stakingProgram.methods
              .collect()
              .accounts(collection.accounts)
              .instruction();

            instructions.push(collectionInstructions);

            const { accounts } = await createUnstakingInstructions(stakingProgram, nft);
            const unstakingInstructions = await stakingProgram.methods
              .unstake()
              .accounts(accounts)
              .instruction();

            instructions.push(unstakingInstructions);
            transaction.instructions.push(...instructions);
          } catch (err) {
            console.error(err);
          }
        })
      );

      if (transaction.instructions.length === 0)
        setError(new Error('transaction failed'));

      return { transaction, error };
    } catch (err) {
      console.error(err);
    }
  };

  return unstakeTx;
};

export default useUnstakeTx;
