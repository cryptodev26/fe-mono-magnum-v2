import { useState } from 'react';

import { NFT, TransactionHook } from 'types';
import { initTransaction, createCollectionInstructions } from 'utils/helpers/web3';

const useCollectTx: TransactionHook = (stakingProvider, stakingProgram, wallet) => {
  const [error, setError] = useState<Error>();

  const collectTx = async (nfts: NFT[]) => {
    try {
      let transaction = await initTransaction(stakingProgram, wallet);
      if (!transaction) throw new Error('transaction could not be initiated');

      await Promise.all(
        nfts.map(async nft => {
          try {
            if (!nft.stakedAccount || !nft.stakedAccountPublicKey)
              throw new Error('no staking account found');

            if (!nft.stakedAccount.rewardCollected) {
              const { accounts, fallbackTx } = await createCollectionInstructions(
                stakingProvider,
                stakingProgram,
                nft
              );

              if (fallbackTx) transaction = fallbackTx;

              const collectionInstructions = await stakingProgram.methods
                .collect()
                .accounts(accounts)
                .instruction();

              transaction.instructions.push(collectionInstructions);
            }
          } catch (err) {
            console.error(err);
          }
        })
      );

      if (transaction.instructions.length === 0)
        setError(new Error('transaction failed'));

      return { transaction, error };
    } catch (err) {
      console.error(err);
    }
  };

  return collectTx;
};

export default useCollectTx;
