import { useState } from 'react';
import toast from 'react-hot-toast';

import { NFT, TransactionType, UseStakingTransaction, Transaction, Keypair } from 'types';
import { usePrograms } from 'contexts';

import useStakeTx from './useStakeTx';
import useUnstakeTx from './useUnstakeTx';
import useCollectTx from './useCollectTx';
import useSignTx from './useSignTx';

const useStakingTransaction: UseStakingTransaction = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const { stakingProvider, stakingProgram, wallet, connection, emit } = usePrograms();

  const stakeTx = useStakeTx(stakingProvider, stakingProgram, wallet);
  const unstakeTx = useUnstakeTx(stakingProvider, stakingProgram, wallet);
  const collectTx = useCollectTx(stakingProvider, stakingProgram, wallet);
  const signTx = useSignTx(wallet, connection);

  const renderToast = (actionType: TransactionType, count: number) => {
    let message = '';

    switch (actionType) {
      case 'stake':
        message = `${count} nft${count > 1 ? 's' : ''} ${
          count > 1 ? 'have' : 'has'
        } been staked`;
        break;
      case 'unstake':
        message = `${count} nft${count > 1 ? 's' : ''} ${
          count > 1 ? 'have' : 'has'
        } been unstaked`;
        break;
      case 'collect':
        message = `Rewards collected for ${count} nft${count > 1 ? 's' : ''}`;
        break;
      default:
        break;
    }

    return toast.success(message);
  };

  const stakingTransaction = async (
    actionType: TransactionType,
    nfts: NFT[],
    lockup?: number
  ) => {
    setLoading(true);
    try {
      if (nfts.length < 1) throw new Error('please select at least one nft to unstake');

      if (!stakingProvider) throw new Error('no provider found');
      if (!stakingProgram) throw new Error('program could not be initiated');

      let action:
        | { transaction: Transaction; signers?: Keypair[]; error?: Error }
        | undefined;

      switch (actionType) {
        case 'stake':
          action = await stakeTx(nfts, lockup);
          emit({ type: 'stake', status: action ? 'success' : 'fail' });
          break;
        case 'unstake':
          action = await unstakeTx(nfts);
          emit({ type: 'unstake', status: action ? 'success' : 'fail' });
          break;
        case 'collect':
          action = await collectTx(nfts);
          break;

        default:
          break;
      }

      if (!action || action.error) return new Error('transaction failed');

      const result = await signTx(action.transaction, action?.signers);

      if (result) renderToast(actionType, nfts.length);
      // transaction hash for solscan transaction.signatures[1].toString()
    } catch (err) {
      toast.error('Transaction failed');
    } finally {
      setLoading(false);
    }
  };

  return { stakingTransaction, loading };
};

export default useStakingTransaction;
