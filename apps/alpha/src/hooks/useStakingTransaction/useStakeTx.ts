import { useState } from 'react';

import hashes from 'data/hashes.json';
import { NFT, TransactionHook, Keypair } from 'types';

import { createStakingInstructions, initTransaction } from 'utils/helpers';

const useStakeTx: TransactionHook = (stakingProvider, stakingProgram, wallet) => {
  const [error, setError] = useState<Error>();

  const stakeTx = async (nfts: NFT[], lockup = 0) => {
    try {
      let transaction = await initTransaction(stakingProgram, wallet);
      if (!transaction) throw new Error('transaction could not be initiated');

      const escrowSigners: Keypair[] = [];

      await Promise.all(
        nfts.map(async nft => {
          try {
            if (!hashes.includes(nft.mint.toString()))
              throw new Error('token is not a magnum nft');

            const { stake, accounts, signers, fallbackTx } =
              await createStakingInstructions(
                stakingProvider,
                stakingProgram,
                nft,
                lockup
              );

            if (fallbackTx) transaction = fallbackTx;

            const instruction = await stakingProgram?.methods
              .stake(stake.lockup, stake.legendary)
              .accounts(accounts)
              .signers(signers)
              .instruction();

            if (!instruction) throw new Error('staking instruction failed');

            transaction.instructions.push(instruction);
            escrowSigners.push(signers[0] as Keypair);
          } catch (err) {
            console.error(err);
          }
        })
      );

      escrowSigners.forEach(signer => {
        transaction.sign(signer);
      });

      if (transaction.instructions.length === 0)
        setError(new Error('transaction failed'));

      return { transaction, signers: escrowSigners, error };
    } catch (err) {
      console.error(err);
    }
  };

  return stakeTx;
};

export default useStakeTx;
