/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';

axios.defaults.baseURL = process.env.NEXT_PUBLIC_API_ORIGIN;
axios.defaults.method = 'GET';

const useAPI = (params: AxiosRequestConfig) => {
  const [response, setResponse] = useState<AxiosResponse>();
  const [error, setError] = useState<AxiosError>();
  const [loading, setLoading] = useState(true);

  const call = async (callParams: AxiosRequestConfig) => {
    try {
      const result = await axios.request({
        ...callParams,
        headers: { api_key: process.env.NEXT_PUBLIC_API_KEY as string },
      });
      setResponse(result);
    } catch (err) {
      setError(err as AxiosError);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    call(params);
  }, []);

  return { response, error, loading };
};

export default useAPI;
