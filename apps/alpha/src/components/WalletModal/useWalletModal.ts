/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useMemo } from 'react';
import { useWallet, Wallet } from '@solana/wallet-adapter-react';
import { useWalletModal } from '@solana/wallet-adapter-react-ui';
import { WalletName, WalletReadyState } from '@solana/wallet-adapter-base';

const useWalletModalLocal = () => {
  const { visible, setVisible } = useWalletModal();
  const { wallets, select } = useWallet();

  const [installedWallets, otherWallets] = useMemo(() => {
    const installed: Wallet[] = [];
    const notDetected: Wallet[] = [];
    const loadable: Wallet[] = [];

    wallets.forEach(wallet => {
      if (wallet.readyState === WalletReadyState.NotDetected) notDetected.push(wallet);
      else if (wallet.readyState === WalletReadyState.Loadable) loadable.push(wallet);
      else if (wallet.readyState === WalletReadyState.Installed) installed.push(wallet);
    });

    return [installed, [...loadable, ...notDetected]];
  }, [wallets]);

  const getStartedWallet = useMemo(() => {
    return installedWallets.length
      ? installedWallets[0]
      : wallets.find(
          (wallet: { adapter: { name: WalletName } }) => wallet.adapter.name === 'Torus'
        ) ||
          wallets.find(
            (wallet: { adapter: { name: WalletName } }) =>
              wallet.adapter.name === 'Phantom'
          ) ||
          wallets.find(
            (wallet: { readyState: WalletReadyState }) =>
              wallet.readyState === WalletReadyState.Loadable
          ) ||
          otherWallets[0];
  }, [installedWallets, wallets, otherWallets]);

  const handleClose = useCallback(() => {
    document.body.style.position = '';
    document.body.style.top = '';
    setVisible(false);
  }, []);

  const handleWalletClick = useCallback(
    (walletName: WalletName) => {
      select(walletName);
      handleClose();
    },
    [select, handleClose]
  );

  return {
    getStartedWallet,
    handleWalletClick,
    installedWallets,
    otherWallets,
    handleClose,
    visible,
  };
};

export default useWalletModalLocal;
