import tw, { styled, css } from 'twin.macro';
import { Button } from 'shared-ui';

const styles = {
  grid: tw`grid grid-cols-6 gap-4`,

  li: tw`m-auto flex flex-col items-center justify-center col-span-2 sm:col-span-1 max-w-[4rem] max-h-[4rem]`,

  option: css`
    ${tw`p-4 rounded-md w-full h-full`}
    & img {
      ${tw`m-auto w-[2rem]`}
    }
  `,
};

const WalletGrid = styled.ul`
  ${styles.grid}
`;

const WalletListItem = styled.li`
  ${styles.li}
`;

const WalletOption = styled(Button)`
  ${styles.option}
`;

export { WalletGrid, WalletListItem, WalletOption };
