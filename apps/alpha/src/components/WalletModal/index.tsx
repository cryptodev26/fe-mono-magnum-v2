import React from 'react';
import { WalletIcon } from '@solana/wallet-adapter-react-ui';
import { WalletReadyState } from '@solana/wallet-adapter-base';
import { Modal, Button, Divider } from 'shared-ui';
import 'twin.macro';

import { WalletGrid, WalletListItem, WalletOption } from './styled';
import useWalletModal from './useWalletModal';

const WalletModal = () => {
  const {
    getStartedWallet,
    handleWalletClick,
    installedWallets,
    otherWallets,
    handleClose,
    visible,
  } = useWalletModal();

  return (
    <Modal
      open={visible}
      handleClose={handleClose}
      heading={installedWallets ? 'Select a wallet' : 'Wallet undetected'}
    >
      {installedWallets.length ? (
        <>
          <WalletGrid>
            {installedWallets.map((wallet, i) => (
              <WalletListItem key={wallet.adapter.name}>
                <WalletOption
                  onClick={() => handleWalletClick(wallet.adapter.name)}
                  variant='primary'
                  type='button'
                  tabIndex={i + 1}
                >
                  <WalletIcon wallet={wallet} />
                </WalletOption>
                {wallet.readyState === WalletReadyState.Installed && (
                  <span>Detected</span>
                )}
              </WalletListItem>
            ))}
          </WalletGrid>
          {otherWallets.length && (
            <>
              <div tw='my-16'>
                <h3>Other wallets</h3>
                <Divider />
                <WalletGrid>
                  {otherWallets.map((wallet, i) => (
                    <WalletListItem key={wallet.adapter.name}>
                      <WalletOption
                        onClick={() => handleWalletClick(wallet.adapter.name)}
                        variant='primary'
                        type='button'
                        tabIndex={i + 1}
                      >
                        <WalletIcon wallet={wallet} />
                      </WalletOption>
                    </WalletListItem>
                  ))}
                </WalletGrid>
              </div>
              <div />
            </>
          )}
        </>
      ) : (
        <Button
          onClick={() => handleWalletClick(getStartedWallet.adapter.name)}
          variant='primary'
          type='button'
        >
          Get started
        </Button>
      )}
    </Modal>
  );
};

export default WalletModal;
