/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useWalletModal, WalletIcon } from '@solana/wallet-adapter-react-ui';
import { Icon } from 'shared-ui';
import 'twin.macro';

import { usePrograms } from 'contexts';
import { formatAddress } from 'utils/helpers/web3';
import useUserStore from 'stores/useUserStore';
import UserBalance from 'components/UserBalance';

import { WalletButtonContent } from './styled';

const useWalletButton = (variant: 'icon' | 'text') => {
  const { setVisible } = useWalletModal();
  const {
    wallet: { publicKey, wallet, disconnect, connect, connecting, connected },
    stakingProgram,
    connection,
  } = usePrograms();
  const {
    balances: { sol, mag, getBalance },
    nfts: { getNfts },
  } = useUserStore();

  const [copied, setCopied] = useState<boolean>(false);
  const [active, setActive] = useState<boolean>(false);

  const base58 = useMemo(() => publicKey?.toBase58(), [publicKey]);

  const content = useMemo(() => {
    if (connecting)
      return (
        <Icon
          variant='spinner'
          tw='w-4 h-4 text-pink-500 animate-spin'
        />
      );
    if (wallet && base58 && connected)
      return (
        <WalletButtonContent>
          <section>
            <WalletIcon wallet={wallet} />
            <span>{formatAddress(base58)}</span>
          </section>
          <section tw='mx-1.5 border-r border-l border-neutral-600'>
            <UserBalance balance='sol' />
          </section>
          <section>
            <UserBalance balance='mag' />
          </section>
        </WalletButtonContent>
      );

    return variant === 'icon' ? (
      <Icon
        variant='wallet'
        tw='text-pink-500'
      />
    ) : (
      <p tw='text-base font-normal text-pink-500'>Connect Wallet</p>
    );
  }, [wallet, base58, connecting, connected, sol, mag]);

  const copyAddress = useCallback(async () => {
    if (base58) {
      await navigator.clipboard.writeText(base58);
      setCopied(true);
      setTimeout(() => setCopied(false), 400);
    }
  }, [base58]);

  const openDropdown = useCallback(() => {
    setActive(true);
  }, []);

  const closeDropdown = useCallback(() => {
    setActive(false);
  }, []);

  const openModal = useCallback(() => {
    setVisible(true);
    closeDropdown();
  }, [closeDropdown]);

  const disconnectWallet = useCallback(() => {
    disconnect();
    closeDropdown();
  }, [closeDropdown, disconnect]);

  const handleClick = useCallback(() => {
    if (!wallet) return setVisible(true);
    if (wallet && !base58)
      return connect()
        .then(() => {
          if (!connected)
            return connect().catch(err => {
              throw new Error(`couldn't connect wallet: ${err}`);
            });
        })
        .catch(err => {
          throw new Error(`couldn't connect wallet: ${err}`);
        });
    openDropdown();
  }, [base58, wallet]);

  useEffect(() => {
    if (publicKey) {
      getBalance(publicKey, connection, 'SOL');
      getBalance(publicKey, connection, 'MAG');
      getNfts(publicKey, connection, 'wallet');
      if (stakingProgram) getNfts(publicKey, connection, 'staked', stakingProgram);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [publicKey]);

  return {
    active,
    copied,
    content,
    copyAddress,
    openModal,
    disconnectWallet,
    handleClick,
    closeDropdown,
  };
};

export default useWalletButton;
