import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Form, Icon, Divider } from 'shared-ui';
import 'twin.macro';

import { useAutoConnect } from 'contexts';

import { useMediaQuery } from '../../hooks/useMediaQuery';

import WalletButton from './WalletButton';
import {
  WalletButtonContainer,
  WalletBtn,
  WalletButtonDropDown,
  WalletButtonDropDownListItem,
} from './styled';

const WalletButtonGroup = () => {
  const ref = useRef<HTMLUListElement>(null);
  const switchRef = useRef<HTMLInputElement>(null);
  const [active, setActive] = useState<boolean>(false);
  const { autoConnect, setAutoConnect } = useAutoConnect();

  const match = useMediaQuery('(max-width: 768px)');

  const autoConnectSwitcher = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setAutoConnect(e.target.checked);
    },
    [setAutoConnect]
  );

  const handleClick = useCallback(() => {
    setActive(true);
  }, []);

  const handleBlur = useCallback(() => {
    setActive(false);
  }, []);

  useEffect(() => {
    const listener = (event: MouseEvent | TouchEvent) => {
      const node = ref.current;

      // Do nothing if clicking dropdown or its descendants
      if (!node || node.contains(event.target as Node)) return;

      handleBlur();
    };

    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handleBlur]);

  return (
    <div tw='w-full flex justify-between items-center'>
      {match && (
        <div tw='ml-auto'>
          <WalletButton />
        </div>
      )}
      <WalletButtonContainer tw='md:ml-auto'>
        <WalletBtn
          aria-expanded={active}
          onClick={handleClick}
          style={{ pointerEvents: active ? 'none' : 'auto' }}
        >
          <Icon
            variant='settings'
            tw='text-pink-500'
          />
        </WalletBtn>
        <WalletButtonDropDown
          active={active}
          aria-label='dropdown-list'
          ref={ref}
          tw='top-14 -left-32'
        >
          <WalletButtonDropDownListItem tw='hover:translate-y-0'>
            <div tw='w-full'>
              <div tw='w-full flex justify-between items-center'>
                <span>Autoconnect</span>
                <Form.Switch
                  ref={switchRef}
                  onChange={autoConnectSwitcher}
                  defaultChecked={autoConnect}
                />
              </div>
              <Divider tw='mb-0 mt-4 mx-auto w-2/3' />
            </div>
          </WalletButtonDropDownListItem>
        </WalletButtonDropDown>
      </WalletButtonContainer>
    </div>
  );
};

export default WalletButtonGroup;
