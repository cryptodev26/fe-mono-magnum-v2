/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef } from 'react';
import { Divider } from 'shared-ui';
import 'twin.macro';

import {
  WalletButtonContainer,
  WalletBtn,
  WalletButtonDropDown,
  WalletButtonDropDownListItem,
} from './styled';
import useWalletButton from './useWalletButton';

const WalletButton = ({ variant = 'icon' }: { variant?: 'icon' | 'text' }) => {
  const ref = useRef<HTMLUListElement>(null);
  const {
    active,
    copied,
    content,
    copyAddress,
    openModal,
    disconnectWallet,
    handleClick,
    closeDropdown,
  } = useWalletButton(variant);

  useEffect(() => {
    const listener = (event: MouseEvent | TouchEvent) => {
      const node = ref.current;

      // Do nothing if clicking dropdown or its descendants
      if (!node || node.contains(event.target as Node)) return;

      closeDropdown();
    };

    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, closeDropdown]);

  return (
    <WalletButtonContainer>
      <WalletBtn
        aria-expanded={active}
        onClick={handleClick}
        style={{ pointerEvents: active ? 'none' : 'auto' }}
      >
        {content}
      </WalletBtn>
      <WalletButtonDropDown
        active={active}
        aria-label='dropdown-list'
        ref={ref}
      >
        <WalletButtonDropDownListItem onClick={copyAddress}>
          <div tw='w-full'>
            {copied ? 'Copied' : 'Copy address'}
            <Divider tw='m-0 mt-2 mx-auto w-2/3' />
          </div>
        </WalletButtonDropDownListItem>
        <WalletButtonDropDownListItem onClick={openModal}>
          <div tw='w-full'>
            Change wallet
            <Divider tw='mb-0 mt-2 mx-auto w-2/3' />
          </div>
        </WalletButtonDropDownListItem>
        <WalletButtonDropDownListItem onClick={disconnectWallet}>
          <div tw='w-full'>
            Disconnect <Divider tw='m-0 mt-2 mx-auto w-2/3' />
          </div>
        </WalletButtonDropDownListItem>
      </WalletButtonDropDown>
    </WalletButtonContainer>
  );
};

export default WalletButton;
