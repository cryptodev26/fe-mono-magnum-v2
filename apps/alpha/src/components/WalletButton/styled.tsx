import tw, { styled, css } from 'twin.macro';
import { Button } from 'shared-ui';

const styles = {
  container: tw`relative inline-block`,

  button: {
    base: tw`p-1.5 sm:p-2 rounded-md hover:translate-y-0 text-2xl`,
    active: tw`text-base py-1`,
  },

  dropDown: {
    base: tw`absolute min-w-[10rem] hidden z-50 bg-neutral-900 ml-2 grid-cols-1 top-3/4 left-0 rounded-md shadow-lg opacity-0 transition-all duration-200`,
    active: tw`opacity-100 translate-y-[10px] grid`,
    li: tw`flex flex-row justify-center items-center cursor-pointer whitespace-nowrap text-center w-full pt-2 rounded-md transition-all duration-200 hover:-translate-y-0.5`,
  },

  content: css`
    ${tw`flex items-center`}
    & section {
      ${tw`flex items-center py-0 px-1.5 my-0`}
    }
    & img,
    svg {
      ${tw`max-w-[1.25rem] min-w-[1rem] mr-1.5`}
    }
    & span {
      ${tw`font-light text-sm`}
    }
  `,
};

const WalletButtonContainer = styled.div`
  ${styles.container}
`;

const WalletBtn = styled(Button)<{ active?: boolean }>(({ active }) => [
  styles.button.base,
  active && styles.button.active,
]);

const WalletButtonDropDown = styled.ul<{ active?: boolean }>(({ active }) => [
  styles.dropDown.base,
  active && styles.dropDown.active,
]);

const WalletButtonDropDownListItem = styled.li`
  ${styles.dropDown.li}
`;

const WalletButtonContent = styled.div`
  ${styles.content}
`;

export {
  WalletButtonContainer,
  WalletBtn,
  WalletButtonDropDown,
  WalletButtonDropDownListItem,
  WalletButtonContent,
};
