import React from 'react';
import { Containers } from 'shared-ui';
import 'twin.macro';

const SkeletonGrid = () => {
  return (
    <Containers.Grid.Base>
      {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(skeleton => (
        <Containers.Grid.Cell
          col={12}
          sm={6}
          md={4}
          lg={3}
          key={skeleton}
          tw='min-w-[250px] max-w-[300px] w-full min-h-[250px] max-h-[300px] h-full rounded-md bg-neutral-800 animate-pulse opacity-60'
        />
      ))}
    </Containers.Grid.Base>
  );
};

export default SkeletonGrid;
