import tw, { styled, css } from 'twin.macro';

const ImageContainer = styled.div((props: { square?: boolean; circle?: boolean }) => [
  tw`relative overflow-hidden w-full transition-all duration-300 ease-in`,

  props.square &&
    css`
      ${tw`before:block`}

      &:before {
        content: '';
        padding-top: 100%;
      }

      & img {
        ${tw`absolute top-1/2 left-1/2 -translate-y-1/2	-translate-x-1/2`}
      }
    `,

  props.circle && tw`rounded-full`,
]);

export default ImageContainer;
