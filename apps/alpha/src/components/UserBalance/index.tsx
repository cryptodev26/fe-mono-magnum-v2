import React, { useCallback } from 'react';

import useUserStore from 'stores/useUserStore';

const UserBalance = ({ balance }: { balance: 'sol' | 'mag' }) => {
  const renderBalance = useCallback((bal: number) => {
    return Number(bal).toFixed(3);
  }, []);

  const {
    balances: { sol, mag },
  } = useUserStore();
  return (
    <>
      {balance === 'sol' ? (
        // <Icon variant='solana' />
        <picture>
          <source
            src='https://cryptologos.cc/logos/solana-sol-logo.svg?v=022'
            type='image/gif'
          />
          <img
            src='https://cryptologos.cc/logos/solana-sol-logo.svg?v=022'
            alt='coin'
          />
        </picture>
      ) : (
        <picture>
          <source
            src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
            type='image/gif'
          />
          <img
            src='https://magnum-images.s3.amazonaws.com/brand/coin.gif'
            alt='coin'
          />
        </picture>
      )}
      <span>{renderBalance(balance === 'sol' ? sol : mag)}</span>
    </>
  );
};

export default UserBalance;
