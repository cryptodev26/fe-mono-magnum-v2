import React from 'react';
import { motion } from 'framer-motion';

const SectionView = (props: {
  title: string;
  position: string;
  text?: string;
  children?: JSX.Element[] | JSX.Element;
  id?: string;
}) => {
  const { id, position, children, text, title } = props;
  return (
    <div
      id={id}
      tw='w-full'
    >
      <div tw='relative flex flex-col justify-center items-center'>
        <div
          tw='absolute top-0 lg:top-3'
          style={{
            left: position === 'left' ? 0 : '',
            right: position === 'right' ? 0 : '',
          }}
        >
          <motion.div
            initial={{ opacity: 0 }}
            animate={{
              opacity: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
              transition: { duration: 0.75, repeat: Infinity, repeatDelay: 3 },
            }}
            tw='my-[6px] self-start'
          >
            <div tw='w-[12px] h-[6px] bg-cyan-primary' />
          </motion.div>
          <motion.div
            initial={{ opacity: 0 }}
            animate={{
              opacity: [0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1],
              transition: {
                duration: 0.75,
                delay: 0.2,
                repeat: Infinity,
                repeatDelay: 3,
              },
            }}
            tw='my-[6px] self-start'
          >
            <div tw='w-[12px] h-[6px] bg-cyan-primary' />
          </motion.div>
          <motion.div
            initial={{ opacity: 0 }}
            animate={{
              opacity: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
              transition: { duration: 0.75, repeat: Infinity, repeatDelay: 3 },
            }}
            tw='my-[6px] self-start'
          >
            <div tw='w-[12px] h-[6px] bg-cyan-primary' />
          </motion.div>
        </div>
        <motion.h1
          initial={{ opacity: 0 }}
          animate={{ opacity: [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1] }}
          transition={{ duration: 0.75, delay: 0.2, repeat: Infinity, repeatDelay: 3 }}
          // tw='text-cyan-primary'
          style={{
            alignSelf: text || 'center',
            // marginRight: props.text === 'end' ? '2rem' : '',
            // marginLeft: props.text === 'start' ? '2rem' : '',
          }}
        >
          {title}
        </motion.h1>
        <div {...props}>{children}</div>
      </div>
    </div>
  );
};

export default SectionView;
