import tw, { styled } from 'twin.macro';

const styles = {
  base: tw`absolute min-w-[10rem] w-full p-2 hidden z-50 bg-neutral-900 ml-2 grid-cols-1 top-full -left-2 rounded-md shadow-lg opacity-0 transition-all duration-200`,

  openUp: tw`top[-320%]`,

  active: tw`opacity-100 translate-y-[10px] grid`,

  container: tw`relative inline-block`,

  listItem: tw`flex flex-row justify-center items-center cursor-pointer whitespace-nowrap text-center w-full pt-2 rounded-md transition-all duration-200 hover:-translate-y-0.5`,
};

const StakedButtonContainer = styled.div`
  ${styles.container}
`;

const StakeButtonDropDown = styled.ul<{ active?: boolean; openUp?: boolean }>(
  ({ active, openUp }) => [styles.base, openUp && styles.openUp, active && styles.active]
);

const StakeButtonDropDownListItem = styled.li`
  ${styles.listItem}
`;

export { StakedButtonContainer, StakeButtonDropDown, StakeButtonDropDownListItem };
