import React, { useCallback, useMemo, useState } from 'react';
import { Icon } from 'shared-ui';

import { NFT, PublicKey, TransactionType } from 'types';
import useUserStore from 'stores/useUserStore';
import { usePrograms } from 'contexts';
import useStakingTransaction from 'hooks/useStakingTransaction';
import 'twin.macro';

const useStakeButton = (variant: TransactionType) => {
  const {
    nfts: { getNfts },
    balances: { getBalance },
  } = useUserStore();
  const {
    wallet: { publicKey },
    connection,
    stakingProgram,
  } = usePrograms();
  const { stakingTransaction, loading } = useStakingTransaction();
  const [active, setActive] = useState<boolean>(false);

  const content = useMemo(() => {
    if (loading)
      return (
        <Icon
          variant='spinner'
          tw='mx-auto w-4 h-4 text-pink-500 animate-spin'
        />
      );

    return `${variant.split('')[0].toUpperCase()}${variant.slice(1)}`;
  }, [loading, variant]);

  const openDropdown = useCallback(() => {
    setActive(true);
  }, []);

  const closeDropdown = useCallback(() => {
    setActive(false);
  }, []);

  const handleTransaction = useCallback(
    async ({ nfts, lockup }: { nfts: NFT[]; lockup?: number }) => {
      switch (variant) {
        case 'stake':
          await stakingTransaction('stake', nfts, lockup);
          break;
        case 'collect':
          await stakingTransaction('collect', nfts);
          break;
        case 'unstake':
          await stakingTransaction('unstake', nfts);
          break;
        default:
          break;
      }
      setTimeout(() => {
        getNfts(publicKey as PublicKey, connection, 'wallet');
        getNfts(publicKey as PublicKey, connection, 'staked', stakingProgram);
        getBalance(publicKey as PublicKey, connection, 'SOL');
        getBalance(publicKey as PublicKey, connection, 'MAG');
      }, 1000);
    },
    [
      connection,
      getBalance,
      getNfts,
      publicKey,
      stakingProgram,
      stakingTransaction,
      variant,
    ]
  );

  const handleLockupSelect = useCallback(
    async (nfts: NFT[], lockup: number) => {
      closeDropdown();
      handleTransaction({ nfts, lockup });
    },
    [closeDropdown, handleTransaction]
  );

  return {
    active,
    openDropdown,
    closeDropdown,
    handleLockupSelect,
    content,
    handleTransaction,
  };
};

export default useStakeButton;
