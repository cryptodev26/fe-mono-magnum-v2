import React, { useCallback, useEffect, useRef } from 'react';
import { Button, Divider } from 'shared-ui';
import 'twin.macro';

import { NFT, TransactionType } from 'types';
import {
  StakedButtonContainer,
  StakeButtonDropDown,
  StakeButtonDropDownListItem,
} from './styled';
import useStakeButton from './useStakingButton';

interface StakeButtonProps {
  nfts: NFT[];
  variant: TransactionType;
}

const StakeButton = ({ nfts, variant, ...props }: StakeButtonProps) => {
  const ref = useRef<HTMLUListElement>(null);

  const {
    openDropdown,
    closeDropdown,
    handleLockupSelect,
    active,
    content,
    handleTransaction,
  } = useStakeButton(variant);

  useEffect(() => {
    const listener = (event: MouseEvent | TouchEvent) => {
      const node = ref.current;

      // Do nothing if clicking dropdown or its descendants
      if (!node || node.contains(event.target as Node)) return;

      closeDropdown();
    };

    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, closeDropdown]);

  const handleClick = useCallback(() => {
    if (variant === 'stake') openDropdown();
    else {
      handleTransaction({ nfts });
    }
  }, [handleTransaction, nfts, openDropdown, variant]);

  return (
    <StakedButtonContainer {...props}>
      <Button
        aria-expanded={active}
        onClick={handleClick}
        tw='w-full'
        style={{ pointerEvents: active ? 'none' : 'auto' }}
      >
        {content}
      </Button>
      {variant === 'stake' && (
        <StakeButtonDropDown
          active={active}
          openUp
          aria-label='dropdown-list'
          ref={ref}
        >
          <StakeButtonDropDownListItem onClick={() => handleLockupSelect(nfts, 0)}>
            <div tw='w-full'>
              1 week
              <Divider tw='mb-0 mt-2 mx-auto w-2/3' />
            </div>
          </StakeButtonDropDownListItem>
          <StakeButtonDropDownListItem onClick={() => handleLockupSelect(nfts, 1)}>
            <div tw='w-full'>
              2 weeks
              <Divider tw='mb-0 mt-2 mx-auto w-2/3' />
            </div>
          </StakeButtonDropDownListItem>
          <StakeButtonDropDownListItem onClick={() => handleLockupSelect(nfts, 2)}>
            <div tw='w-full'>
              4 weeks
              <Divider tw='mb-0 mt-2 mx-auto w-2/3' />
            </div>
          </StakeButtonDropDownListItem>
        </StakeButtonDropDown>
      )}
    </StakedButtonContainer>
  );
};

export default StakeButton;
export type { StakeButtonProps };
