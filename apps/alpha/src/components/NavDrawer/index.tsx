import React from 'react';
import 'twin.macro';

import WalletButtonGroup from 'components/WalletButton/WalletButtonGroup';

const NavDrawer = () => {
  return (
    <div tw='p-4'>
      <WalletButtonGroup />
    </div>
  );
};

export default NavDrawer;
