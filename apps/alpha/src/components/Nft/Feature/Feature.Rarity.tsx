import React from 'react';
import { Containers } from 'shared-ui';
import 'twin.macro';

import { NFT } from 'types';
import useNftFeature from './useNftFeature';

// TODO: need to revisit this once we are passing magnums
// temporarily using hardcoded value for view in development

// const Rarity = () => {
const Rarity = ({ nft }: { nft: NFT }) => {
  const { attributeRarityMatch } = useNftFeature();

  return (
    <Containers.Grid.Base tw='w-full mt-10'>
      {attributeRarityMatch(nft).map(attr => (
        <Containers.Grid.Cell
          col={6}
          sm={4}
          lg={3}
          key={attr?.trait_type}
          tw='border border-neutral-500 rounded-md p-2.5 text-center text-sm'
        >
          <p tw='font-bold'>{attr?.trait_type}</p>
          <p>{attr?.value}</p>
          <p>{attr?.rating}%</p>
        </Containers.Grid.Cell>
      ))}
    </Containers.Grid.Base>
  );
};

export default Rarity;
