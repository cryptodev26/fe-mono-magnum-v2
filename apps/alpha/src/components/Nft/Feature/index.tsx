import React, { useEffect } from 'react';
import { Badge, Containers, SocialButton, Spinner } from 'shared-ui';
import 'twin.macro';

import useNftFeature from './useNftFeature';
import Rarity from './Feature.Rarity';

const NftFeature = () => {
  const {
    getToken,
    featuredNft: { token, isStaked },
    renderStakeAction,
    queryNft,
    nfts,
  } = useNftFeature();

  useEffect(() => {
    const get = async () => {
      await getToken();
    };
    get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryNft, nfts]);

  if (!token.mint)
    return (
      <div tw='fixed top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2'>
        <Spinner />
      </div>
    );

  return (
    <div>
      <div tw='flex flex-col items-center gap-4 lg:(flex-row justify-between items-start)'>
        <div>
          <Containers.Img
            featured
            tw='w-[fit-content] max-w-[300px] pointer-events-none'
          >
            <picture>
              <source
                src={token.metadata.image}
                type='image/gif'
              />
              <img
                src={token.metadata.image}
                alt={token.mint.toString()}
              />
            </picture>
          </Containers.Img>
          <div tw='flex flex-wrap mt-3 gap-2'>
            {isStaked && <Badge variant='primary'>Staked</Badge>}
            {token.isLegendary && <Badge variant='primary'>Legendary</Badge>}
          </div>
        </div>
        <div tw='w-full flex flex-col gap-4 mt-5 max-w-[400px]'>
          <SocialButton
            icon='magicEden'
            link={`https://www.magiceden.io/item-details/${token.mint.toString()}`}
          >
            View on Magic Eden
          </SocialButton>
          <SocialButton
            icon='openSea'
            link={`https://opensea.io/assets/solana/${token.mint.toString()}`}
          >
            View on Open Sea
          </SocialButton>
          <SocialButton
            icon='solScan'
            link={`https://solscan.io/token/${token.mint.toString()}`}
          >
            View on Solscan
          </SocialButton>
          {renderStakeAction()}
        </div>
      </div>
      {!token.isLegendary && <Rarity nft={token} />}
    </div>
  );
};

export default NftFeature;
