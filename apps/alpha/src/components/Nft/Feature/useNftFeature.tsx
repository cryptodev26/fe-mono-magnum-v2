import React, { useCallback, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { PublicKey } from '@solana/web3.js';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import sameOrAfter from 'dayjs/plugin/isSameOrAfter';
import 'twin.macro';

import { NFT, StaticAttributes } from 'types';
import { usePrograms, useStaticApi } from 'contexts';
import { getMetaDataByMint } from 'utils/helpers/web3/getMetaData';
import useUserStore from 'stores/useUserStore';
import StakeButton from 'components/StakeButton';
import legendaries from 'data/legendaries.json';

dayjs.extend(relativeTime);
dayjs.extend(sameOrAfter);

const useNftFeature = () => {
  const [queryNft, setQueryNft] = useState<string | null>(null);
  const [featuredNft, setFeaturedNft] = useState<{
    isOwner: boolean;
    isStaked: boolean;
    token: NFT;
  }>({ isOwner: false, isStaked: false, token: {} as NFT });

  const { query } = useRouter();
  useEffect(() => {
    if (query.nft) setQueryNft(query.nft as string);
  }, [query]);

  const { nfts } = useUserStore();
  const { connection, loading, stakingProgram } = usePrograms();

  const {
    data: {
      genesis: { attributes },
    },
  } = useStaticApi();

  // check if nft is owned by user and return, using user's nft object if so
  const userOwnedNftObj = [...nfts.wallet, ...nfts.staked].find(
    t => t.mint.toString() === query.nft
  );

  // check if token is currently staked
  const isStaked = async () => {
    if (!queryNft) return;

    const stakingProgramAccounts = await stakingProgram.account.stakeAccount.all();

    return stakingProgramAccounts
      .map(s => s.account.stakingMint.toString())
      .includes(queryNft);
  };

  const renderStakeAction = useCallback(() => {
    if (!userOwnedNftObj) return;

    const now = dayjs();
    const unstakeDate = dayjs.unix(
      Number(featuredNft.token.stakedAccount?.unstakeDate.toString())
    );
    const stakeDate = dayjs.unix(
      Number(featuredNft.token.stakedAccount?.created.toString())
    );
    const countdown = dayjs().to(unstakeDate, true);

    if (!featuredNft.isStaked || countdown.includes('NaN'))
      return (
        <StakeButton
          variant='stake'
          nfts={[featuredNft.token]}
        />
      );

    if (featuredNft.token.stakedAccount?.isV2) {
      const lastCollected = dayjs.unix(
        featuredNft.token.stakedAccount.lastRewardCollection.toNumber()
      );
      const timeSinceStake = now.diff(stakeDate) / 1000 / 60 / 60;
      const timeSinceColl = now.diff(lastCollected) / 1000 / 60 / 60;

      if (
        (!lastCollected && timeSinceStake >= 24) ||
        (lastCollected && timeSinceColl >= 24)
      ) {
        return (
          <StakeButton
            variant='collect'
            nfts={[featuredNft.token]}
          />
        );
      }
    }

    return now.isAfter(unstakeDate) ? (
      <StakeButton
        variant='unstake'
        nfts={[featuredNft.token]}
      />
    ) : (
      <h6 tw='mt-3 mx-auto'>Unstake in {countdown}</h6>
    );
  }, [featuredNft, userOwnedNftObj]);

  const getToken = useCallback(async () => {
    if (loading || !queryNft) return;

    const isLegendary = legendaries.includes(queryNft);
    const staked = (await isStaked()) as boolean;

    if (!userOwnedNftObj) {
      const mint = new PublicKey(queryNft);
      const { metadata } = await getMetaDataByMint(connection, mint);

      setFeaturedNft({
        isOwner: false,
        isStaked: staked,
        token: { mint, metadata: metadata.data, isLegendary } as NFT,
      });
    } else
      setFeaturedNft({
        isOwner: true,
        isStaked: staked,
        token: userOwnedNftObj,
      });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connection, loading, queryNft, nfts, renderStakeAction, featuredNft]);

  // this puts the attributes returned from our nft's metadata object aginst the attributes in our db to get our internally defined rarity rating
  const attributeRarityMatch = (nft: NFT) => {
    return nft.metadata.attributes.map(attr => {
      const type = attr.trait_type
        .toLowerCase()
        .split(' ')
        .join('_') as keyof StaticAttributes['data'];

      return (
        attributes[type] && attributes[type].find(trait => trait.value === attr.value)
      );
    });
  };

  // Omega's real name is Nick
  return {
    attributeRarityMatch,
    getToken,
    renderStakeAction,
    featuredNft,
    connection,
    loading,
    queryNft,
    nfts,
  };
};

export default useNftFeature;
