import React from 'react';
import { Containers, Animate, Badge } from 'shared-ui';
import 'twin.macro';

import { NFT } from 'types';

const {
  Grid: { Cell },
  Img,
} = Containers;
const { parallex, easyTransition } = Animate;

interface NftThumbnailProps {
  nft: NFT;
  featured?: boolean;
  glow?: boolean;
  badge?: boolean;
  onClick?(T: unknown): void;
}

const NftTile = React.forwardRef(function NftTile(
  { nft, featured, glow, badge, ...props }: NftThumbnailProps,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _ref
) {
  const {
    stakedAccount,
    metadata: { image, name },
  } = nft;

  return (
    <Cell
      variants={parallex}
      transition={easyTransition}
      initial='hidden'
      whileInView='visible'
      col={12}
      sm={6}
      md={4}
      lg={3}
    >
      <Img
        featured={featured}
        glow={glow}
        {...props}
        tw='cursor-pointer'
      >
        {badge && stakedAccount && <Badge tw='absolute right-2.5 top-2.5'>staked</Badge>}
        <picture>
          <source
            src={image}
            type='image/gif'
          />
          <img
            src={image}
            alt={name}
          />
        </picture>
      </Img>
    </Cell>
  );
});

export default NftTile;
export type { NftThumbnailProps };
