export const attributes = [
  {
    trait_type: 'Background',
    value: 'Mechanical',
  },
  {
    trait_type: 'Headgear',
    value: 'Brainiac',
  },
  {
    trait_type: 'Costume',
    value: 'Magnum Fighter',
  },
  {
    trait_type: 'Glasses',
    value: 'Googly Eyes',
  },
  {
    trait_type: 'Face Gear',
    value: 'Rebreather Cyborg',
  },
  {
    trait_type: 'Body',
    value: 'Cyborg',
  },
  {
    trait_type: 'Face',
    value: 'Face',
  },
];
