import React from 'react';
import Group from './group';
import Label from './label';
import Input from './input';
import Select from './select';
// import CheckBox from './checkbox';
// import TextArea from './textArea';
import Button from './button';

// const Form = { Group, Label, Input, Select, CheckBox, TextArea, Button };
const Form = { Group, Label, Input, Select, Button };

export const InputGroup = (props: {
  type: string;
  name: string;
  placeholder: string;
  onChange: (e: React.FormEvent<HTMLInputElement>) => void;
  required: boolean;
}) => {
  const { name, required, type, placeholder, onChange } = props;
  return (
    <div tw='px-3 col-span-1'>
      <Group check={false}>
        <Label
          htmlFor={name}
          group
          check={false}
        >
          {name} {required && <span tw='text-red-primary'>*</span>}
        </Label>
        <Input
          type={type}
          name={name}
          placeholder={placeholder}
          onChange={onChange}
        />
      </Group>
    </div>
  );
};

export default Form;
