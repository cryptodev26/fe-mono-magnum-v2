import tw, { styled, css } from 'twin.macro';

const Input = styled.input`
  ${css`
    ${tw`w-full p-3 rounded-md shadow-sm uppercase`}

    &:focus-visible, :focus, :active {
      ${tw`border outline-none shadow-xl`}
    }
  `}
`;

export default Input;
