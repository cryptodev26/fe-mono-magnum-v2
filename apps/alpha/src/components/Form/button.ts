import tw, { styled, css } from 'twin.macro';

const Button = styled.button(
  (props: { mint?: boolean; success?: boolean; danger?: boolean; general?: boolean }) => [
    css`
      ${tw`block uppercase rounded-lg p-3 text-center transition-all duration-300 ease-in-out hover:shadow-lg`}
      &[disabled] {
        ${tw`border-gray-800 text-gray-800`}
      }
    `,

    // props.disabled && tw`border-gray-800 text-gray-100`,

    props.mint &&
      css`
        ${tw`border min-w-[100px] py-2`}
        background-image: linear-gradient(
          to right,
          rgba(22, 230, 231, 0) 50%,
          var(--cyan-primary) 51%,
          var(--cyan-primary) 100%);
        background-size: 200% auto;
        font-weight: 900 !important;

        &:hover {
          ${tw`shadow-xl`}
          background-position: right center;
        }
      `,
    props.success &&
      css`
        background-image: linear-gradient(
          to right,
          rgba(22, 230, 231, 0) 50%,
          var(--yellow-secondary) 51%,
          var(--yellow-primary) 100%
        );
        background-size: 200% auto;
        font-weight: 900 !important;

        &:hover {
          background-position: right center;
        }
      `,

    props.danger &&
      css`
        background-image: linear-gradient(
          to right,
          var(--red-secondary) 50%,
          var(--red-primary) 51%,
          var(--red-secondary) 100%
        );
        background-size: 200% auto;
        color: var(--black-primary);

        &:hover {
          background-position: right center;
        }
      `,

    props.general &&
      css`
        ${tw`text-white p-1 transition-all duration-200 ease-in w-full p-2 m-0`}
        font-weight: 900 !important;
        background-image: linear-gradient(
          to right,
          rgba(22, 230, 231, 0) 50%,
          var(--black-primary) 51%,
          var(--black-secondary) 100%
        );
        background-size: 200% auto;
        font-weight: 900 !important;

        &:hover {
          background-position: right center;
        }
      `,
  ]
);

export default Button;
