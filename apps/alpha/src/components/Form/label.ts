import tw, { styled } from 'twin.macro';

const Label = styled.label((props: { group?: boolean; check?: boolean }) => [
  props.group && tw`inline-block mb-2`,
  props.check && tw`flex flex-row-reverse`,
]);

export default Label;
