import React, { forwardRef } from 'react';
import tw, { styled, css } from 'twin.macro';
import Input from './input';

interface CheckProps {
  name: string;
}

const CheckInput: any = styled(Input)`
  ${css`
    ${tw`relative cursor-pointer h-10 w-20 p-4 appearance-none bg-red-secondary checked:bg-yellow-secondary transition duration-300 after:(absolute content-['NO'] p-4 h-10 w-10 flex justify-center items-center rounded-md bg-black-secondary border border-white top-0 left-0 transform transition duration-300) checked:after:(content-['YES'] scale-110 translate-x-10)`}
  `}
`;

const CheckBox = forwardRef<HTMLInputElement, CheckProps>((props, checkRef) => {
  return (
    <div>
      <CheckInput
        ref={checkRef}
        {...props}
        id={props.name}
        name={props.name}
        type='checkbox'
      />
    </div>
  );
});

export default CheckBox;
