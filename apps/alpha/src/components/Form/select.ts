import tw, { styled, css } from 'twin.macro';

const Select = styled.select`
  ${css`
    ${tw`w-full py-2 px-1 rounded-md bg-gray-800 shadow-sm uppercase`}

    &:focus-visible, :focus, :active {
      ${tw`border border-yellow-50 outline-none shadow-xl`}
    }
  `}
`;

export default Select;
