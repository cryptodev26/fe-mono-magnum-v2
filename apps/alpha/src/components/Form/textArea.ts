import tw, { styled, css } from 'twin.macro';

const TextArea = styled.textarea`
  ${css`
    ${tw`w-full p-3 rounded-md bg-black-secondary shadow-sm`}

    &:focus-visible, :focus, :active {
      ${tw`border border-yellow-primary outline-none shadow-xl`}
    }
  `}
`;

export default TextArea;
