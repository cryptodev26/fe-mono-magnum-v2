import tw, { styled } from 'twin.macro';

const Group = styled.div((props: { check?: boolean }) => [
  tw`mb-4`,

  props.check && tw`mb-8 flex flex-row-reverse justify-end items-center`,
]);

export default Group;
