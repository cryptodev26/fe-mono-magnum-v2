import React, { FC } from 'react';
import { motion } from 'framer-motion';
import { Animate } from 'shared-ui';
import 'twin.macro';

const ActionBanner: FC<{ children: JSX.Element }> = ({ children }) => {
  return (
    <motion.div
      variants={Animate.slide}
      initial='hidden'
      animate='visible'
      exit='hidden'
      transition={Animate.easyTransition}
      tw='fixed left-0 bottom-0 w-full bg-neutral-800 border-t border-neutral-700 flex justify-center p-6'
    >
      {children}
    </motion.div>
  );
};

export default ActionBanner;
