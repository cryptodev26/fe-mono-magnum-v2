import React, { createContext, PropsWithChildren, useReducer } from 'react';
import dataReducer from './store.reducer';
import { IDataState } from './store.types';

const INITIAL_STATE: IDataState = {
  loading: true,
  nft: {
    attributes: {
      backgrounds: [],
      faces: [],
      bodies: [],
      face_gear: [],
      glasses: [],
      costumes: [],
      head_gear: [],
      mannequin: [],
    },
    legendaries: [],
  },
  team: [],
  content: {
    logo: {
      small: '',
      large: '',
    },
    lore: {
      banner: '',
      figure: '',
    },
    roadmap: {
      banner: {
        evolution: '',
        stasis: '',
        token: '',
      },
    },
    arcade: '',
    footer: {
      banner: '',
    },
    bobble: '',
    trailer: {
      video: '',
      poster: '',
    },
  },
  stakingTransactions: [],
};

export const DataContext = createContext<{
  state: IDataState;
  dispatch: React.Dispatch<any>;
}>({
  state: INITIAL_STATE,
  dispatch: () => null,
});

interface DataProviderPros {
  children: any;
}

export const DataProvider: React.FC<PropsWithChildren<DataProviderPros>> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(dataReducer, INITIAL_STATE);
  const dataProviderValue = React.useMemo(() => ({ state, dispatch }), [state, dispatch]);

  return (
    <DataContext.Provider value={dataProviderValue}>{children}</DataContext.Provider>
  );
};
