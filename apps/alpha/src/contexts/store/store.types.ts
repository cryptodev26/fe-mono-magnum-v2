export interface IAttribute {
  _id?: string;
  name?: string;
  data?: string;
  rating: number;
  __v: string;
}

export interface ILegendary {
  _id?: string;
  name?: string;
  image?: string;
  __v?: number;
}

export interface ITeam {
  _id?: string;
  name?: string;
  title?: string;
  image?: string;
  __v?: number;
}

export interface IStakingTransaction {
  user?: string;
  tx?: string;
  action?: string;
  mint?: string;
  rewards?: number;
  _id?: string;
  createdAt?: string;
  updatedAt?: string;
  __v?: 0;
}
export interface IDataState {
  loading: boolean;
  nft: {
    attributes: {
      backgrounds: IAttribute[];
      faces: IAttribute[];
      bodies: IAttribute[];
      face_gear: IAttribute[];
      glasses: IAttribute[];
      costumes: IAttribute[];
      head_gear: IAttribute[];
      mannequin: IAttribute[];
    };
    legendaries: ILegendary[];
  };
  team: ITeam[];
  content: {
    logo: {
      small: string;
      large: string;
    };
    lore: {
      banner: string;
      figure: string;
    };
    roadmap: {
      banner: {
        evolution: string;
        stasis: string;
        token: string;
      };
    };
    arcade: string;
    footer: {
      banner: string;
    };
    bobble: string;
    trailer: {
      video: string;
      poster: string;
    };
  };
  stakingTransactions: IStakingTransaction[];
}
