import React, { createContext, FC, ReactNode, useContext, useMemo } from 'react';

// import { useAPI } from 'hooks';
import { StaticApiContextState } from 'types';
import attributes from './temp/attributes.json';

const StaticApiContext = createContext<StaticApiContextState>(
  {} as StaticApiContextState
);

function useStaticApi(): StaticApiContextState {
  return useContext(StaticApiContext);
}

const StaticApiProvider: FC<{ children: ReactNode }> = ({ children }) => {
  // const attributes = useAPI({ url: '/genesis/attributes' });

  const value = useMemo(
    () => ({
      data: {
        genesis: {
          attributes: {
            background: attributes.data.backgrounds,
            body: attributes.data.bodies,
            costume: attributes.data.costumes,
            face_gear: attributes.data.face_gear,
            face: attributes.data.faces,
            glasses: attributes.data.glasses,
            headgear: attributes.data.head_gear,
          },
        },
      },
    }),
    []
  );

  return <StaticApiContext.Provider value={value}>{children}</StaticApiContext.Provider>;
};

export default StaticApiProvider;
export { useStaticApi };
