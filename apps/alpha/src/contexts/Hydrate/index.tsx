import React, {
  ReactNode,
  useContext,
  useEffect,
  createContext,
  useMemo,
  useState,
} from 'react';

const Hydrated = createContext<boolean>(false);

export function useHydration() {
  return useContext(Hydrated);
}

export default function HydrationProvider({ children }: { children: ReactNode }) {
  const [isHydrated, setIsHydrated] = useState<boolean>(false);

  useEffect(() => {
    setIsHydrated(true);
  }, []);

  const value = useMemo(() => isHydrated, [isHydrated]);

  return <Hydrated.Provider value={value}>{children}</Hydrated.Provider>;
}
