import React, { createContext, FC, ReactNode, useContext } from 'react';
import { useLocalStorage } from '@solana/wallet-adapter-react';

interface AutoConnectContextState {
  autoConnect: boolean;
  setAutoConnect(autoConnect: boolean): void;
}

const AutoConnectContext = createContext<AutoConnectContextState>(
  {} as AutoConnectContextState
);

function useAutoConnect(): AutoConnectContextState {
  return useContext(AutoConnectContext);
}

const AutoConnectProvider: FC<{ children: ReactNode }> = ({ children }) => {
  // TODO: fix auto connect to actual reconnect on refresh/other.
  // TODO: make switch/slider settings
  // const [autoConnect, setAutoConnect] = useLocalStorage('autoConnect', false);
  const [autoConnect, setAutoConnect] = useLocalStorage('autoConnect', true);

  return (
    // eslint-disable-next-line react/jsx-no-constructed-context-values
    <AutoConnectContext.Provider value={{ autoConnect, setAutoConnect }}>
      {children}
    </AutoConnectContext.Provider>
  );
};

export default AutoConnectProvider;
export { useAutoConnect };
export type { AutoConnectContextState };
