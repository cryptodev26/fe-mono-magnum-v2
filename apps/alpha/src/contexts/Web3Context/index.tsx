import React, { FC, ReactNode, useCallback, useMemo } from 'react';
import { WalletAdapterNetwork, WalletError } from '@solana/wallet-adapter-base';
import { ConnectionProvider, WalletProvider } from '@solana/wallet-adapter-react';
import {
  PhantomWalletAdapter,
  SolflareWalletAdapter,
  SolletExtensionWalletAdapter,
  SolletWalletAdapter,
  TorusWalletAdapter,
} from '@solana/wallet-adapter-wallets';
// import { clusterApiUrl } from '@solana/web3.js';

import AutoConnectProvider, { useAutoConnect } from './AutoConnectProvider';
import ProgramProvider, { usePrograms } from './ProgramProvider';
import WalletModalProvider from './WalletModalProvider';

const WalletContextProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const { autoConnect } = useAutoConnect();

  const network = 'mainnet-beta' as WalletAdapterNetwork; // set env variable
  const endpoint =
    // 'https://solana-api.syndica.io/access-token/OwQf5h3qfv3bREKZhLWJgke7sYxpfe1rWRaDH5QXoSS1YCC2PYrPnxGSKXhPLezk/rpc';
    'https://solana-api.syndica.io/access-token/gDcBtRpIMF8CTsmbM2wlxntQpKP2J77WBUE6gsnBm946I3E6O9MIziwYlwctOMbj/rpc';
  const wallets = useMemo(
    () => [
      new PhantomWalletAdapter(),
      new SolflareWalletAdapter(),
      new SolletWalletAdapter({ network }),
      new SolletExtensionWalletAdapter({ network }),
      new TorusWalletAdapter(),
    ],
    [network]
  );

  const onError = useCallback((error: WalletError) => {
    console.error(error); // come up with something better
  }, []);

  return (
    // TODO: add wallet modal provider around `children`
    <ConnectionProvider endpoint={endpoint}>
      <WalletProvider
        wallets={wallets}
        onError={onError}
        autoConnect={autoConnect}
      >
        <WalletModalProvider>
          <ProgramProvider>{children}</ProgramProvider>
        </WalletModalProvider>
      </WalletProvider>
    </ConnectionProvider>
  );
};

const Web3Provider: FC<{ children: ReactNode }> = ({ children }) => {
  return (
    <AutoConnectProvider>
      <WalletContextProvider>{children}</WalletContextProvider>
    </AutoConnectProvider>
  );
};

export default Web3Provider;
export { useAutoConnect, usePrograms };
