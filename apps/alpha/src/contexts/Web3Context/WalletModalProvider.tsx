import React, { FC, useMemo, useState } from 'react';
import { WalletModalContext } from '@solana/wallet-adapter-react-ui';
import { AnimatePresence } from 'framer-motion';

import WalletModal from 'components/WalletModal';
import { styles } from 'styles';

const WalletModalProvider: FC<{ children: JSX.Element }> = ({ children }) => {
  const [visible, setVisible] = useState<boolean>(false);

  const value = useMemo(() => ({ visible, setVisible }), [visible, setVisible]);

  return (
    <WalletModalContext.Provider value={value}>
      <>
        {children}
        <AnimatePresence>
          {visible && (
            <div css={styles.container}>
              <WalletModal />
            </div>
          )}
        </AnimatePresence>
      </>
    </WalletModalContext.Provider>
  );
};

export default WalletModalProvider;
