import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { useConnection, useWallet } from '@solana/wallet-adapter-react';
import { toast } from 'react-hot-toast';

import { Connection, StakingProgram, StakingProvider, WalletContextState } from 'types';
import { formatAddress, initProgram, initProvider } from 'utils/helpers/web3';

interface ProgramContextState {
  loading: boolean;
  error: Error | null;
  stakingProvider: StakingProvider;
  stakingProgram: StakingProgram;
  wallet: WalletContextState;
  connection: Connection;
  event?: {
    type: 'stake' | 'unstake';
    status: 'success' | 'fail';
  };
  emit(event: { type: 'stake' | 'unstake'; status: 'success' | 'fail' }): void;
}

const StakingProgramContext = createContext<ProgramContextState>(
  {} as ProgramContextState
);

function usePrograms(): ProgramContextState {
  return useContext(StakingProgramContext);
}

const ProgramProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const wallet = useWallet();
  const { connection } = useConnection();
  const [stakingProgram, setProgram] = useState<ProgramContextState['stakingProgram']>();
  const [stakingProvider, setProvider] =
    useState<ProgramContextState['stakingProvider']>();
  const [loading, setLoading] = useState<ProgramContextState['loading']>(false);
  const [error, setError] = useState<ProgramContextState['error']>(null);

  const [event, setEvent] = useState<ProgramContextState['event']>();

  const emitEventResult = useCallback(({ type, status }: any) => {
    setEvent({ type, status });
  }, []);

  useEffect(() => {
    const init = async () => {
      try {
        setLoading(true);
        const newProvider = await initProvider(connection, wallet);
        const newProgram = await initProgram(newProvider);

        setProvider(newProvider);
        setProgram(newProgram);
      } catch (err) {
        setError(err as Error);
      } finally {
        setLoading(false);
      }
    };

    init();
  }, [connection, wallet]);

  useEffect(() => {
    if (wallet.connected)
      toast.success(
        `${formatAddress(wallet.publicKey?.toString() as string)} is connected with ${
          wallet.wallet?.adapter.name
        } wallet`
      );
  }, [wallet.connected, wallet.publicKey, wallet.wallet?.adapter.name]);

  const value = useMemo(
    () =>
      ({
        stakingProvider,
        stakingProgram,
        wallet,
        connection,
        loading,
        error,
        event,
        emit: emitEventResult,
      } as ProgramContextState),
    [
      stakingProgram,
      stakingProvider,
      wallet,
      connection,
      loading,
      error,
      event,
      emitEventResult,
    ]
  );

  return (
    <StakingProgramContext.Provider value={value}>
      {children}
    </StakingProgramContext.Provider>
  );
};

export default ProgramProvider;
export { usePrograms };
export type { ProgramContextState };
