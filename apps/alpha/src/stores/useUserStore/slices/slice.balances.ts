import { SetState } from 'zustand';

import { User, UserBalances } from 'types';
import { getUserMagBalance, getUserSolBalance } from 'utils/helpers/web3';

const createBalancesSlice: (set: SetState<User>) => UserBalances = set => ({
  sol: 0,
  mag: 0,

  getBalance: async (publicKey, connection, token) => {
    try {
      if (token === 'SOL') {
        const sol = await getUserSolBalance(publicKey, connection);
        set(state => ({ ...state, balances: { ...state.balances, sol } }));
      }

      const mag = await getUserMagBalance(publicKey, connection);
      set(state => ({ ...state, balances: { ...state.balances, mag } }));
    } catch (e) {
      console.error(`Error getting ${token} balance. ${e}`);
    }
  },
});

export default createBalancesSlice;
