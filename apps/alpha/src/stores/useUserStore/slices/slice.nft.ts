import { SetState } from 'zustand';

import { User, UserNFTs } from 'types';
import { getStakedNfts, getWalletNfts } from 'utils/helpers/web3';

const createNftsSlice: (set: SetState<User>) => UserNFTs = set => ({
  wallet: [],
  staked: [],

  getNfts: async (publicKey, connection, from, program) => {
    try {
      if (from === 'staked') {
        if (!program) return console.error('Need to pass program');

        const staked = await getStakedNfts(publicKey, connection, program);

        set(state => ({
          ...state,
          nfts: {
            ...state.nfts,
            staked,
          },
        }));
      }

      const wallet = await getWalletNfts(publicKey, connection);

      set(state => ({
        ...state,
        nfts: {
          ...state.nfts,
          wallet,
        },
      }));
    } catch (e) {
      console.error(`Error getting ${from} NFTs. ${e}`);
    }
  },
});

export default createNftsSlice;
