import { SetState } from 'zustand';

import { User, UserProto } from 'types';

const createProtoSlice: (set: SetState<User>) => UserProto = set => ({
  setWallet: wallet => {
    set(state => ({ ...state, wallet }));
  },

  setConnection: connection => set(state => ({ ...state, connection })),
});

export default createProtoSlice;
