import create from 'zustand';

import { Connection, User, WalletContextState } from 'types';

import { createBalancesSlice, createNftsSlice, createProtoSlice } from './slices';

const useUserStore = create<User>(set => ({
  userWallet: {} as WalletContextState,
  connection: {} as Connection,
  balances: createBalancesSlice(set),
  nfts: createNftsSlice(set),
  _proto_: createProtoSlice(set),
}));

export default useUserStore;
