// eslint-disable-next-line no-undef
module.exports = {
    purge: [],
    darkMode: false, // or 'media' or 'class'
    theme: {
      extend: {
        colors: {
          black: {
            primary: 'var(--black-primary)',
            secondary: 'var(--black-secondary)',
            ternary: 'var(--black-ternary)',
          },
          grey: { primary: 'var(--grey-primary)', secondary: 'var(--grey-secondary)' },
          white: 'var(--white-primary)',
          orange: 'var(--orange-primary)',
          cyan: { primary: 'var(--cyan-primary)', secondary: 'var(--cyan-secondary)' },
          pink: { primary: 'var(--pink-primary)', secondary: 'var(--pink-secondary)' },
          red: { primary: 'var(--red-primary)', secondary: 'var(--red-secondary)' },
          yellow: { primary: 'var(--yellow-primary)', secondary: 'var(--yellow-secondary)' },
          purple: { primary: 'var(--purple-primary)', secondary: 'var(--purple-secondary)' },
          socials: {
            discord: 'var(--discord)',
            twitter: 'var(--twitter)',
            instagram: 'var(--instagram)',
            linkedin: 'var(--linkedin)',
            twitch: 'var(--twitch)',
          },
        },
        fontFamily: {
          play: ['var(--font-game)', 'sans-serif'],
          oswald: ['var(--font-oswald)', 'sans-serif'],
          primary: ['var(--font-primary)', 'sans-serif'],
          neon: ['var(--font-neon)', 'sans-serif'],
        },
        width: {
          fit: 'fit-content',
        },
        height: {
          screen: {
            full: '100vh',
            '1/2': '50vh',
            '1/4': '25vh',
            '2/4': '50vh',
            '3/4': '75vh',
            '1/3': '33.33333vh',
            '2/3': '66.66667vh',
            '1/5': '20vh',
            '2/5': '40vh',
            '3/5': '60vh',
            '4/5': '80vh',
            '1/6': '16.66667vh',
            '2/6': '33.33333vh',
            '3/6': '50vh',
            '4/6': '66.66667vh',
            '5/6': '75vh',
            twothird: '83.3333333333vh',
          },
        },
        boxShadow: {
          neon: {
            pink: '0 0 0.1em var(--white-primary), 0 0 0.2em var(--white-primary), 0 0 0.3em var(--white-primary), 0 0 0.5em rgba(255, 0, 185, 0.3),0 0 0.7em rgba(255, 0, 185, 0.3), 0 0 0.9em rgba(255, 0, 185, 0.3), 0 0 1.1em rgba(255, 0, 185, 0.3), 0 0 1.2em rgba(255, 0, 185, 0.3), inset 0 0 0.1em #fff, inset 0 0 0.2em #fff, inset 0 0 0.3em #fff, inset 0 0 0.4em #fe36c7, inset 0 0 0.6em rgba(255, 0, 185, 0.3), inset 0 0 0.8em rgba(255, 0, 185, 0.3), inset 0 0 1em rgba(255, 0, 185, 0.3), inset 0 0 1.2em rgba(255, 0, 185, 0.3)',
            blue: '0 0 0.5em rgba(0,255,255,0.3),0 0 0.7em rgba(0,255,255,0.3),0 0 0.3em rgba(0,255,255,0.3),0 0 1.1em rgba(0,255,255,0.3),0 0 1.2em rgba(0,255,255,0.3)',
          },
        },
      },
    },
    variants: {
      extend: {},
    },
    plugins: [],
  };
  